﻿using Evince.PassengersTransport.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace Evince.PassengersTransport.Domain.Tests
{
    [TestClass]
    public class RoadMapTests
    {
        readonly Journey journey = new Journey(
            new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
            new Terminal(2, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
            TimeSpan.FromMinutes(10)
            );

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddRouteItem_IfJourneyIsInvalid_ThenThrowsException()
        {
            var roadMap = new RoadMap();

            Journey journey = null;

            roadMap.AddRouteItem(journey);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void AddRouteItem_IfJourneyAlreadyExistAtRoadmap_ThenThrowsException()
        {
            var roadMap = new RoadMap();

            roadMap.AddRouteItem(journey);

            roadMap.AddRouteItem(journey);
        }

        [TestMethod]
        public void AddRouteItem_IfParametersAreValid_ThenSucceeds()
        {
            var roadMap = new RoadMap();

            roadMap.AddRouteItem(journey);

            var journey2 = new Journey(
                new Terminal(3, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                new Terminal(4, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );

            roadMap.AddRouteItem(journey2);

            Assert.IsTrue(roadMap.Items.Count == 2);
            Assert.IsTrue(roadMap.Items.Any(i => i.Journey == journey));
            Assert.IsTrue(roadMap.Items.Any(i => i.Journey == journey2));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveRouteItem_IfJourneyIsInvalid_ThenThrowsException()
        {
            var roadMap = new RoadMap();

            Journey journey = null;

            roadMap.RemoveRouteItem(journey);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void RemoveRouteItem_IfRoadMapItemIsInvalid_ThenThrowsException()
        {
            var roadMap = new RoadMap();

            RoadMapItem item = null;

            roadMap.RemoveRouteItem(item);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RemoveRouteItem_IfJourneyDoesNotExists_ThenThrowsException()
        {
            var roadMap = new RoadMap();

            roadMap.AddRouteItem(journey);

            var journey2 = new Journey(
                new Terminal(3, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                new Terminal(4, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );

            roadMap.RemoveRouteItem(journey2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void RemoveRouteItem_IfRoadMapItemDoesNotExists_ThenThrowsException()
        {
            var roadMap = new RoadMap();

            var item = new RoadMapItem(0, journey);

            roadMap.AddRouteItem(item);

            var journey2 = new Journey(
                new Terminal(3, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                new Terminal(4, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );

            var item2 = new RoadMapItem(1, journey2);

            roadMap.RemoveRouteItem(item2);
        }

        [TestMethod]
        public void RemoveRouteItem_IfParametersAreValid_ThenSucceeds()
        {
            var roadMap = new RoadMap();

            var item = new RoadMapItem(0, journey);

            roadMap.AddRouteItem(item);

            var journey2 = new Journey(
                new Terminal(3, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                new Terminal(4, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );

            var item2 = new RoadMapItem(1, journey2);

            roadMap.AddRouteItem(item2);

            roadMap.RemoveRouteItem(item);

            Assert.IsTrue(roadMap.Items.Count == 1);
            Assert.IsTrue(roadMap.Items.Any(i => i.Journey == item2.Journey));
        }
    }
}
