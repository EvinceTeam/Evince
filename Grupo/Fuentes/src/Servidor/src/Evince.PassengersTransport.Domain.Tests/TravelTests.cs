﻿using Evince.PassengersTransport.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Evince.PassengersTransport.Domain.Tests
{
    [TestClass]
    public class TravelTests
    {
        readonly Driver driver = new Driver(Guid.NewGuid().ToString(), "John", "Doe");
        readonly Bus bus = new Bus("AB123", 60);
        readonly Journey journey = new Journey(
            new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
            new Terminal(2, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
            TimeSpan.FromMinutes(10)
            );
        readonly RoadMap roadmap = new RoadMap();

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfStipulatedStartTimeIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(-1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfNumberOfPassengersIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                -1,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfNumberOfPassangersIsGreaterThanBusCapacity_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                200,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_IfRoadmapIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                null,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_IfJourneyIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                null,
                driver,
                bus,
                new Route(roadmap)
                );
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_IfDriverIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                null,
                bus,
                new Route(roadmap)
                );
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_IfBusIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                null,
                new Route(roadmap)
                );
        }

        [TestMethod]
        public void Create_IfParametersAreValid_ThenSucceeds()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            Assert.IsNotNull(travel);
            Assert.AreNotEqual(travel.Id, Guid.Empty);
            Assert.AreEqual(travel.State, TravelState.Pending);
            Assert.AreEqual(travel.DriverFullname, driver.Fullname);
        }

        [TestMethod]
        public void Create_IfSameJourneyAndStart_ThenSucceeds()
        {
            var now = DateTime.Now;

            var travel1 = new Travel(
                now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            var travel2 = new Travel(
                now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            Assert.AreEqual(travel1, travel2);
            Assert.IsTrue(travel1 == travel2);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MarkTravelStart_IfStateIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            travel.MarkTravelStart();

            Assert.AreEqual(travel.State, TravelState.InProgress);
            Assert.IsNotNull(travel.StartTime);

            travel.MarkTravelStart(); // throws InvalidOperationException
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void MarkTravelEnd_IfStateIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            travel.MarkTravelEnd();
        }

        [TestMethod]
        public void MarkTravel_IfStatesAreValid_ThenSucceeds()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            travel.MarkTravelStart();

            Assert.AreEqual(travel.State, TravelState.InProgress);
            Assert.IsNotNull(travel.StartTime);

            travel.MarkTravelEnd();

            Assert.AreEqual(travel.State, TravelState.Finalized);
            Assert.IsNotNull(travel.EndTime);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void ReplaceDriver_IfStateIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );
            travel.MarkTravelStart();

            Assert.AreEqual(travel.State, TravelState.InProgress);

            travel.ReplaceDriver(new Driver(Guid.NewGuid().ToString(), "Juan", "Perez"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ReplaceDriver_IfDriverIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            travel.ReplaceDriver(null);
        }

        [TestMethod]
        public void ReplaceDriver_IfDriverIsValid_ThenSucceeds()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            travel.ReplaceDriver(new Driver(Guid.NewGuid().ToString(), "Juan", "Perez"));

            Assert.AreNotEqual(travel.DriverId, driver.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ReplaceBus_IfDriverIsInvalid_ThenThrowsException()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            travel.ReplaceBus(null);
        }

        [TestMethod]
        public void ReplaceBus_IfDriverIsValid_ThenSucceeds()
        {
            var travel = new Travel(
                DateTime.Now.AddHours(1),
                null,
                null,
                20,
                roadmap,
                journey,
                driver,
                bus,
                new Route(roadmap)
                );

            travel.ReplaceBus(new Bus("ZZZ123", 50));

            Assert.AreNotEqual(travel.BusId, bus.Id);
        }
    }
}
