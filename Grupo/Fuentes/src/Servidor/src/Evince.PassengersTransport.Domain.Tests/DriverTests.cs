﻿using Evince.PassengersTransport.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Evince.PassengersTransport.Domain.Tests
{
    [TestClass]
    public class DriverTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfIdIsInvalid_ThenThrowsException()
        {
            var driver = new Driver("", "John", "Doe");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfNameIsEmpty_ThenThrowsException()
        {
            var driver = new Driver(Guid.NewGuid().ToString(), "", "Doe");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfLastnameIsEmpty_ThenThrowsException()
        {
            var driver = new Driver(Guid.NewGuid().ToString(), "John", "");
        }

        [TestMethod]
        public void Create_IfParametersAreValid_ThenSucceeds()
        {
            var driver = new Driver(Guid.NewGuid().ToString(), "John", "Doe");

            Assert.IsNotNull(driver);
            Assert.AreNotEqual(Guid.Empty, driver.Id);
        }
    }
}
