﻿using Evince.PassengersTransport.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Evince.PassengersTransport.Domain.Tests
{
    [TestClass]
    public class BusTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfIdIsInvalid_ThenThrowsException()
        {
            var bus = new Bus("", 50);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfCapacityIsInvalid_ThenThrowsException()
        {
            var bus = new Bus("AB123", 0);
        }

        [TestMethod]
        public void Create_IfParametersAreValid_ThenSucceeds()
        {
            var bus = new Bus("AB123", 50);

            Assert.IsNotNull(bus);
            Assert.AreNotEqual(string.Empty, bus.Id);
        }
    }
}
