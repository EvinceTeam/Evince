﻿using Evince.PassengersTransport.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Evince.PassengersTransport.Domain.Tests
{
    [TestClass]
    public class TerminalTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfIdEqualsZero_ThenThrowsException()
        {
            var terminal = new Terminal(0, "Sarasa");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_IfIdGreaterThanMax_ThenThrowsException()
        {
            var maxValue = int.MaxValue;

            var terminal = new Terminal(maxValue + 1, "Sarasa");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfNameIsEmpty_ThenThrowsException()
        {
            var terminal = new Terminal(1, "");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfNameIsInvlaid_ThenThrowsException()
        {
            var terminal = new Terminal(1, null);
        }

        [TestMethod]
        public void Create_IfParametersAreValid_ThenSucceeds()
        {
            var terminal = new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)");

            Assert.IsNotNull(terminal);
            Assert.AreEqual(terminal.Id, 1);
        }

        [TestMethod]
        public void Entity_IfCodeIsValid_ThenSucceeds()
        {
            var terminal = new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)");

            Assert.AreEqual(terminal.Code, "BUE");
            Assert.AreEqual(terminal.Name, "Buenos Aires. Terminal Retiro");
            Assert.AreEqual(terminal.ShortDescription, "(BUE) Buenos Aires. Terminal Retiro");
        }
    }
}
