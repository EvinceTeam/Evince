﻿using Evince.PassengersTransport.Domain.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Evince.PassengersTransport.Domain.Tests
{
    [TestClass]
    public class JourneyTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_IfStartNodeIsInvalid_ThenThrowsException()
        {
            var journey = new Journey(null, new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"), TimeSpan.FromMinutes(10));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Create_IfEndNodeIsInvalid_ThenThrowsException()
        {
            var journey = new Journey(new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"), null, TimeSpan.FromMinutes(10));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Create_IfStartAndEndNodeAreEquals_ThenThrowsException()
        {
            var journey = new Journey(
                new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );
        }

        [TestMethod]
        public void Create_IfNodesAreValid_ThenSucceeds()
        {
            var journey = new Journey(
                new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                new Terminal(2, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );

            Assert.AreEqual(1, journey.StartNode.Id);
            Assert.AreEqual(2, journey.EndNode.Id);
        }

        [TestMethod]
        public void Create_SameValuesAreTheSameJourny_ThenSucceeds()
        {
            var journey1 = new Journey(
                new Terminal(1,"(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                new Terminal(2, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );
            var journey2 = new Journey(
                new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                new Terminal(2, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );

            Assert.AreEqual(journey1, journey2);

            var journey3 = new Journey(
                new Terminal(2, "(MDP) Mar del Plata Terminal (Buenos Aires) (Argentina)"),
                new Terminal(1, "(BUE) Buenos Aires. Terminal Retiro (Capital Federal) (Argentina)"),
                TimeSpan.FromMinutes(10)
                );

            Assert.AreEqual(journey1, journey3);
            Assert.AreEqual(journey2, journey3);
        }
    }
}
