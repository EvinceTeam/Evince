﻿using Evince.UsersManagement.Application.Interfaces;
using Evince.Application.Interfaces.Persistance;
using Evince.Persistance.EFCore;
using Evince.Persistance.Identity;
using Evince.Persistance.Identity.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;

namespace Evince.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                try
                {
                    using (var context = (EvinceDbContext)scope.ServiceProvider.GetService<IEvinceDbContext>())
                    {
                        context.Database.Migrate();
                        EvinceInitializer.Initialize(context);
                    }

                    using (var context = scope.ServiceProvider.GetService<EvinceIdentityDbContext>())
                    {
                        context.Database.Migrate();

                        using (var userService = (UserService)scope.ServiceProvider.GetService<IUserService>())
                        {
                            EvinceIdentityInitializer.Initialize(context, userService);
                        }
                    }
                }
                catch (Exception ex)
                {
                    var logger = scope.ServiceProvider.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while migrating or initializing the databases.");
                }
            }

            host.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                          .AddJsonFile($"appsettings.Local.json", optional: true, reloadOnChange: true)
                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true);
                    config.AddEnvironmentVariables();
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddDebug();
                })
                .UseStartup<Startup>();
    }
}
