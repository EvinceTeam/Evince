﻿using Evince.Persistance.Identity;
using Evince.Persistance.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Evince.API.Installers
{
    public class IdentityInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            // Use SQL Database if in Azure, otherwise, use Local SQL Server Provider
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production")
            {
                services.AddDbContext<EvinceIdentityDbContext>(options =>
                      options.UseSqlServer(configuration.GetConnectionString("EvinceIdentityDatabase")));
            }
            else
            {
                services.AddDbContext<EvinceIdentityDbContext>(options =>
                      options.UseSqlServer(configuration.GetConnectionString("EvinceIdentityDatabaseDev")));
            }

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<EvinceIdentityDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 6;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireLowercase = false;

                    // Lockout settings
                    options.Lockout.MaxFailedAccessAttempts = 10;

                    // User settings
                    options.User.RequireUniqueEmail = false;

                    // Default SignIn settings.
                    options.SignIn.RequireConfirmedEmail = false;
                })
                .ConfigureApplicationCookie(options =>
                {
                    // Cookie settings
                    options.Cookie.HttpOnly = true;
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                    options.LoginPath = "/login";
                    options.AccessDeniedPath = "/error";
                    options.SlidingExpiration = true;
                });
        }
    }
}
