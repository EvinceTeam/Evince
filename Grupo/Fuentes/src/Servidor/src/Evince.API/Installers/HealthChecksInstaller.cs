﻿using Evince.Persistance.EFCore;
using Evince.Persistance.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Evince.API.Installers
{
    public class HealthChecksInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddHealthChecks()
                //.AddDbContextCheck<EvinceDbContext>() TODO: checkear contexto de Evince DB
                .AddDbContextCheck<EvinceIdentityDbContext>("Database access health check");
        }
    }
}
