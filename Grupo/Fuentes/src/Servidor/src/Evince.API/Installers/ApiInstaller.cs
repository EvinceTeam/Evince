﻿using Evince.Administration.Application.Subscriptions.Commands.UpdateSubscription;
using Evince.API.Filters;
using Evince.Common.Events;
using Evince.PassengersTransport.Application.Drivers.Commands.GenerateDriverTravels;
using Evince.UsersManagement.Application.Users.Commands.Register;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Evince.API.Installers
{
    public class ApiInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

            services
                .AddMvc(options => options.Filters.Add(typeof(CustomExceptionFilterAttribute)))
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressMapClientErrors = true;
                })
                .AddFluentValidation(fv =>
                {
                    fv.RegisterValidatorsFromAssemblyContaining<RegisterUserCommandValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<GenerateDriverTravelsCommandValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<UpdateSubscriptionCommandValidator>();
                    fv.RegisterValidatorsFromAssemblyContaining<UserWithRoleDriverCreatedNotificationValidator>();
                });
        }
    }
}
