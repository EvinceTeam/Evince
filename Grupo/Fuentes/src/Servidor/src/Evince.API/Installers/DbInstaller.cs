﻿using Evince.Application.Interfaces.Persistance;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.Persistance.EFCore;
using Evince.Persistance.EFCore.EntitiesReadOnlyRepositories;
using Evince.Persistance.EFCore.EntitiesRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Evince.API.Installers
{
    public class DbInstaller : IInstaller
    {
        public void InstallServices(IServiceCollection services, IConfiguration configuration)
        {
            // Use SQL Database if in Azure, otherwise, use Local SQL Server Provider
            if (Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") == "Production")
            {
                services.AddDbContext<IEvinceDbContext, EvinceDbContext>(options =>
                    options.UseSqlServer(configuration.GetConnectionString("EvinceDatabase")));

                services.AddDbContext<EvinceDbContext, EvinceDbContext>(options =>
                    options.UseSqlServer(configuration.GetConnectionString("EvinceDatabase")));
            }
            else
            {
                services.AddDbContext<IEvinceDbContext, EvinceDbContext>(options =>
                   options.UseSqlServer(configuration.GetConnectionString("EvinceDatabaseDev")));

                services.AddDbContext<EvinceDbContext, EvinceDbContext>(options =>
                   options.UseSqlServer(configuration.GetConnectionString("EvinceDatabaseDev")));
            }

            services.AddTransient<IReadOnlyEvinceDbContext, ReadOnlyEvinceDbContext>();
            services
                .AddScoped(typeof(IRepository<,>), typeof(EfRepository<,>))
                .AddScoped(typeof(IAsyncRepository<,>), typeof(EfRepository<,>))
                .AddScoped(typeof(IEfRepository<,>), typeof(EfRepository<,>))
                .AddScoped(typeof(IReadOnlyRepository<,>), typeof(EfReadOnlyRepository<,>))
                .AddScoped(typeof(IAsyncReadOnlyRepository<,>), typeof(EfReadOnlyRepository<,>))
                .AddScoped(typeof(IEfReadOnlyRepository<,>), typeof(EfReadOnlyRepository<,>))
                .AddScoped<IAdministratorRepository, AdministratorRepository>()
                .AddScoped<IAdministratorReadOnlyRepository, AdministratorReadOnlyRepository>()
                .AddScoped<IBusRepository, BusRepository>()
                .AddScoped<IBusReadOnlyRepository, BusReadOnlyRepository>()
                .AddScoped<IDelayRepository, DelayRepository>()
                .AddScoped<IDelayReadOnlyRepository, DelayReadOnlyRepository>()
                .AddScoped<IDriverRepository, DriverRepository>()
                .AddScoped<IDriverReadOnlyRepository, DriverReadOnlyRepository>()
                .AddScoped<IJourneyRepository, JourneyRepository>()
                .AddScoped<IJourneyReadOnlyRepository, JourneyReadOnlyRepository>()
                .AddScoped<IRoadMapItemRepository, RoadMapItemRepository>()
                .AddScoped<IRoadMapItemReadOnlyRepository, RoadMapItemReadOnlyRepository>()
                .AddScoped<IRoadMapRepository, RoadMapRepository>()
                .AddScoped<IRoadMapReadOnlyRepository, RoadMapReadOnlyRepository>()
                .AddScoped<ISubscriptionRepository, SubscriptionRepository>()
                .AddScoped<ISubscriptionReadOnlyRepository, SubscriptionReadOnlyRepository>()
                .AddScoped<ITerminalRepository, TerminalRepository>()
                .AddScoped<ITerminalReadOnlyRepository, TerminalReadOnlyRepository>()
                .AddScoped<ITravelRepository, TravelRepository>()
                .AddScoped<ITravelReadOnlyRepository, TravelReadOnlyRepository>()
                .AddScoped<IRouteRepository, RouteRepository>()
                .AddScoped<IRouteReadOnlyRepository, RouteReadOnlyRepository>()
                .AddScoped<IAssignmentRepository, AssignmentRepository>()
                .AddScoped<IAssignmentReadOnlyRepository, AssignmentReadOnlyRepository>()
                .AddScoped<IDriverAssignmentRepository, DriverAssignmentRepository>()
                .AddScoped<IDriverAssignmentReadOnlyRepository, DriverAssignmentReadOnlyRepository>();
        }
    }
}
