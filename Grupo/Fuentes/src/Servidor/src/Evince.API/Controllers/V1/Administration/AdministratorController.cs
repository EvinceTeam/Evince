﻿using Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMap;
using Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMaps;
using Evince.Administration.Application.Administrators.Queries.GetAdministratorSuscribedRoadMaps;
using Evince.Administration.Application.Administrators.Queries.GetRoadMapsByNodes;
using Evince.Administration.Application.Administrators.Queries.GetSuscribedRoadMapsByNodes;
using Evince.Administration.Application.Subscriptions.Commands.UpdateSubscription;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Evince.API.Controllers.V1.Administration
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AdministratorController : BaseController
    {
        /// <summary>
        /// Actualiza una suscripcion a un recorrido
        /// </summary>
        [HttpPost("update/{roadmap}")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Subscription(int roadmap)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new UpdateSubscriptionCommand { AdministratorId = idClaim.Value, RoadmapId = roadmap }));
        }

        /// <summary>
        /// Obtiene todos los recorridos
        /// </summary>
        [HttpGet("recorridos")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los recorridos", typeof(IEnumerable<Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMaps.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> RoadMaps()
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetAdministratorRoadMapsQuery { AdministratorId = idClaim.Value }));
        }

        /// <summary>
        /// Obtiene un recorrido especifico
        /// </summary>
        [HttpGet("recorridos/{roadmap}")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Recorrido", typeof(Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMap.RoadMapDto))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> RoadMaps(int roadmap)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetAdministratorRoadMapQuery { AdministratorId = idClaim.Value, RoadMapId = roadmap }));
        }

        /// <summary>
        /// Obtiene todos los recorridos a los que se esta suscrito
        /// </summary>
        [HttpGet("recorridos/subscribed")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los recorridos", typeof(IEnumerable<Evince.Administration.Application.Administrators.Queries.GetAdministratorSuscribedRoadMaps.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> SubscribedRoadMaps()
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetAdministratorSuscribedRoadMapsQuery { AdministratorId = idClaim.Value }));
        }

        /// <summary>
        /// Obtiene todos los recorridos que tienen un origen particular
        /// </summary>
        [HttpGet("recorridos/origen/{startNode}")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los recorridos", typeof(IEnumerable<Evince.Administration.Application.Administrators.Queries.GetRoadMapsByNodes.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> RoadMapsStartNode(int startNode)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetRoadMapsByNodesQuery { AdministratorId = idClaim.Value, StartNode = startNode }));
        }

        /// <summary>
        /// Obtiene todos los recorridos que tienen un destino particular
        /// </summary>
        [HttpGet("recorridos/destino/{endNode}")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los recorridos", typeof(IEnumerable<Evince.Administration.Application.Administrators.Queries.GetRoadMapsByNodes.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> RoadMapsEndNode(int endNode)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetRoadMapsByNodesQuery { AdministratorId = idClaim.Value, EndNode = endNode }));
        }

        /// <summary>
        /// Obtiene todos los recorridos que tienen un origen y destino particular
        /// </summary>
        [HttpGet("recorridos/origenydestino/{startNode}&{endNode}")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los recorridos", typeof(IEnumerable<Evince.Administration.Application.Administrators.Queries.GetRoadMapsByNodes.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> RoadMaps(int startNode, int endNode)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetRoadMapsByNodesQuery { AdministratorId = idClaim.Value, StartNode = startNode, EndNode = endNode }));
        }

        /// <summary>
        /// Obtiene todos los recorridos suscritos que tienen un origen particular
        /// </summary>
        [HttpGet("recorridos/origensuscribed/{startNode}")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los recorridos", typeof(IEnumerable<Evince.Administration.Application.Administrators.Queries.GetSuscribedRoadMapsByNodes.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> SuscribedRoadMapStartNode(int startNode)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetSuscribedRoadMapsByNodesQuery { AdministratorId = idClaim.Value, StartNode = startNode }));
        }

        /// <summary>
        /// Obtiene todos los recorridos suscritos que tienen un destino particular
        /// </summary>
        [HttpGet("recorridos/destinosuscribed/{endNode}")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los recorridos", typeof(IEnumerable<Evince.Administration.Application.Administrators.Queries.GetSuscribedRoadMapsByNodes.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> SuscribedRoadMapEndNode(int endNode)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetSuscribedRoadMapsByNodesQuery { AdministratorId = idClaim.Value, EndNode = endNode }));
        }

        /// <summary>
        /// Obtiene todos los recorridos suscritos que tienen un origen y destino particular
        /// </summary>
        [HttpGet("recorridos/origenydestinosuscribed/{startNode}&{endNode}")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Consultant")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los recorridos", typeof(IEnumerable<Evince.Administration.Application.Administrators.Queries.GetSuscribedRoadMapsByNodes.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> SubscribedRoadMap(int startNode, int endNode)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetSuscribedRoadMapsByNodesQuery { AdministratorId = idClaim.Value, StartNode = startNode, EndNode = endNode }));
        }
    }
}
