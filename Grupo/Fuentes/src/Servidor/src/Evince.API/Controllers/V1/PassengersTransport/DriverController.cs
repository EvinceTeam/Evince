﻿using Evince.PassengersTransport.Application.Drivers.Queries.GetDriverLastTravels;
using Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMap;
using Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMaps;
using Evince.PassengersTransport.Application.Drivers.Queries.GetDriverTravels;
using Evince.PassengersTransport.Application.Travels.Commands.AddDelay;
using Evince.PassengersTransport.Application.Travels.Commands.EndTravel;
using Evince.PassengersTransport.Application.Travels.Commands.StartTravel;
using Evince.PassengersTransport.Application.Travels.Queries.GetTravel;
using Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelays;
using Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelayTypes;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Evince.API.Controllers.V1.PassengersTransport
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class DriverController : BaseController
    {
        /// <summary>
        /// Obtiene viajes
        /// </summary>
        [HttpGet("viajes")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK, "Todos los viajes o uno específico", typeof(IEnumerable<Evince.PassengersTransport.Application.Drivers.Queries.GetDriverTravels.TravelDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Travels([FromQuery]int? travel)
        {
            if (travel.HasValue)
            {
                return Ok(await Mediator.Send(new GetTravelQuery { TravelId = travel.Value }));
            }

            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetDriverTravelsQuery { DriverId = idClaim.Value }));
        }

        /// <summary>
        /// Obtiene los ultimos n viajes del chofer logueado
        /// </summary>
        [HttpGet("viajes/ultimos")]
        [Produces("application/json")]
        [Authorize(Roles = "Driver")]
        [SwaggerResponse(StatusCodes.Status200OK, "Viajes de un chofer", typeof(IEnumerable<Evince.PassengersTransport.Application.Drivers.Queries.GetDriverLastTravels.TravelDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Travel(int? amount)
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetDriverLastTravelsQuery { DriverId = idClaim.Value, Amount = amount }));
        }

        /// <summary>
        /// Obtiene los recorridos del chofer logueado
        /// </summary>
        [HttpGet("recorridos")]
        [Produces("application/json")]
        [Authorize(Roles = "Driver")]
        [SwaggerResponse(StatusCodes.Status200OK, "Recorridos de un chofer", typeof(IEnumerable<Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMaps.RoadMapDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> RoadMaps()
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetDriverRoadMapsQuery { DriverId = idClaim.Value }));
        }

        /// <summary>
        /// Obtiene el recorrido actual o siguiente del chofer logueado
        /// </summary>
        [HttpGet("recorrido")]
        [Produces("application/json")]
        [Authorize(Roles = "Driver")]
        [SwaggerResponse(StatusCodes.Status200OK, "Recorridos de un chofer", typeof(Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMap.RoadMapDto))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> RoadMap()
        {
            var idClaim = HttpContext.User.Claims.Single(claim => claim.Type.Equals("id"));

            return Ok(await Mediator.Send(new GetDriverRoadMapQuery { DriverId = idClaim.Value }));
        }

        /// <summary>
        /// Marca el inicio de un viaje
        /// </summary>
        [HttpPut("viajes/start")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Start([FromBody]StartTravelCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Marca el fin de un viaje
        /// </summary>
        [HttpPut("viajes/stop")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Stop([FromBody]EndTravelCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Agrega un delay a un viaje
        /// </summary>
        [HttpPost("retrasos")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Delays([FromBody]AddDelayCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Obtiene retrasos
        /// </summary>
        [HttpGet("retrasos")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK, "Obtiene los retrasos", typeof(Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelays.DelayDto))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Delays([FromQuery]int? travel)
        {
            if (travel.HasValue)
            {
                return Ok(await Mediator.Send(new GetTravelDelaysQuery { TravelId = travel.Value }));
            }

            return Ok(await Mediator.Send(new GetTravelDelayTypesQuery()));
        }
    }
}