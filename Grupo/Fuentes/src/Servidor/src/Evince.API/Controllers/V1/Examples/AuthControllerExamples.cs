﻿using Evince.Common.Entities;
using Evince.UsersManagement.Application.Users.Commands.Login;
using Evince.UsersManagement.Application.Users.Commands.Logout;
using Evince.UsersManagement.Application.Users.Commands.Register;
using Evince.UsersManagement.Application.Models;
using Swashbuckle.AspNetCore.Filters;
using System.Collections.Generic;

namespace Evince.API.Controllers.V1.Examples
{
    public class RegisterUserCommandExample : IExamplesProvider<RegisterUserCommand>
    {
        public RegisterUserCommand GetExamples()
        {
            return new RegisterUserCommand
            {
                Username = "demo",
                Password = "demo123",
                Lastname = "Gomez",
                Name = "Juan",
                Role = Roles.Driver
            };
        }
    }

    public class LoginCommandRequestExample : IExamplesProvider<LoginCommand>
    {
        public LoginCommand GetExamples()
        {
            return new LoginCommand
            {
                Username = "demo",
                Password = "demo123",
                Remember = true
            };
        }
    }

    public class LoginCommandResponseExample : IExamplesProvider<UserLoginDto>
    {
        public UserLoginDto GetExamples()
        {
            return new UserLoginDto
            {
                RefreshToken = "914a05fe-0e38-46f7-8799-ac321f2fcc24",
                Token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkZW1vIiwianRpIjoiODQyM2VkYzktMDM4Mi00NTAzLWI4ZjgtNjFmZGRlNTIxNTljIiwibmFtZWlkIjoiZGVtbyIsInVuaXF1ZV9uYW1lIjoiZGVtbyIsImlkIjoiYzA1ZjljODktY2Y1MC00ZDBhLWEwNzUtNjdhZmNkNTk5MDIwIiwibmJmIjoxNTc4MjQ2MDA0LCJleHAiOjE1NzgyNDk2MDQsImlhdCI6MTU3ODI0NjAwNH0.yZKhvwGH_AazLUOlfimbY7Sx4Tw_vpLKmxEdYprL6bI",
                Username = "demo",
                Rol = 1,
                RolName = "Driver"
            };
        }
    }

    public class LogoutCommandExample : IExamplesProvider<LogoutCommand>
    {
        public LogoutCommand GetExamples()
        {
            return new LogoutCommand();
        }
    }

    public class GetUserRolesQueryExample : IExamplesProvider<IEnumerable<RoleDto>>
    {
        public IEnumerable<RoleDto> GetExamples()
        {
            return new RoleDto[]
            {
                new RoleDto { Id = 1, Name = "Admin" },
                new RoleDto { Id = 2, Name = "Driver" }
            };
        }
    }
}
