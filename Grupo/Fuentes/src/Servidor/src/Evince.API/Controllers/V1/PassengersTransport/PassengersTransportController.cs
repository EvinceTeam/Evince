﻿using Evince.PassengersTransport.Application.Assignments.CreateAssignment;
using Evince.PassengersTransport.Application.Drivers.Commands.GenerateRandomDriverTravels;
using Evince.PassengersTransport.Application.Drivers.Queries.GetDrivers;
using Evince.PassengersTransport.Application.Journeys.Commands.CreateJourney;
using Evince.PassengersTransport.Application.Journeys.Queries.GetJourneys;
using Evince.PassengersTransport.Application.RoadMaps.Commands.CreateRoadMap;
using Evince.PassengersTransport.Application.Terminals.Queries.GetTerminalList;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Evince.API.Controllers.V1.PassengersTransport
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PassengersTransportController : BaseController
    {
        /// <summary>
        /// Obtiene el listado de todas las terminales disponibles en Argentina
        /// </summary>
        /// <param name="query">Full collection filter by this value</param>
        [HttpGet("terminales")]
        [Produces("application/json")]
        [SwaggerResponse(StatusCodes.Status200OK, "Listado de todas las terminales disponibles en Argentina", typeof(IEnumerable<TerminalDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Terminals([FromQuery]string query)
        {
            return Ok(await Mediator.Send(new GetTerminalListQuery { Query = query }));
        }

        /// <summary>
        /// Obtiene el listado de todos los trayectos disponibles
        /// </summary>
        [HttpGet("trayectos")]
        [Authorize(Roles = "Admin,Driver")]
        [Produces("application/json")]
        [SwaggerResponse(StatusCodes.Status200OK, "listado de todos los trayectos disponibles", typeof(IEnumerable<JourneyDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Journeys([FromQuery]string query)
        {
            return Ok(await Mediator.Send(new GetJourneysQuery { Query = query }));
        }

        /// <summary>
        /// Crea viajes random para un chofer
        /// </summary>
        [HttpPost("viajes")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Travels([FromBody]string driverId)
        {
            driverId = driverId ?? HttpContext.User.Claims.Single(claim => claim.Type.Equals("id")).Value;

            return Ok(await Mediator.Send(new GenerateRandomDriverTravelsCommand { DriverId = driverId }));
        }

        /// <summary>
        /// Crea un trayecto
        /// </summary>
        [HttpPost("trayecto")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Journey([FromBody]CreateJourneyCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Crea un recorrido
        /// </summary>
        [HttpPost("recorrido")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> RoadMap([FromBody]CreateRoadMapCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Obtiene el listado de todos los choferes
        /// </summary>
        [HttpGet("choferes")]
        [Authorize(Roles = "Admin,Driver")]
        [Produces("application/json")]
        [SwaggerResponse(StatusCodes.Status200OK, "listado de todos los choferes", typeof(IEnumerable<DriverDto>))]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Drivers([FromQuery]string query)
        {
            return Ok(await Mediator.Send(new GetDriversQuery { Query = query }));
        }

        /// <summary>
        /// Asigna un usuario a un recorrido
        /// </summary>
        [HttpPost("asignar")]
        [Produces("application/json")]
        [Authorize(Roles = "Admin,Driver")]
        [SwaggerResponse(StatusCodes.Status200OK)]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Unauthorized")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Assign([FromBody]CreateAssignmentCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}