﻿using Evince.UsersManagement.Application.Users.Commands.Login;
using Evince.UsersManagement.Application.Users.Commands.Logout;
using Evince.UsersManagement.Application.Users.Commands.Register;
using Evince.UsersManagement.Application.Models;
using Evince.UsersManagement.Application.Users.Queries.GetRoles;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Collections.Generic;
using System.Threading.Tasks;
using Evince.UsersManagement.Application.Users.Commands.RefreshToken;

namespace Evince.API.Controllers.V1
{
    [AllowAnonymous]
    public class AuthController : BaseController
    {
        /// <summary>
        /// Registers an user
        /// </summary>
        /// <param name="command">
        /// User data to be registered
        /// 
        /// Roles:
        /// 1- Admin
        /// 2- Driver
        /// 3- Consultant
        /// </param>
        [HttpPost("register")]
        [Produces("application/json")]
        [SwaggerResponse(StatusCodes.Status200OK, "Returns a successful response")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "There was a validation problem")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Register([FromBody]RegisterUserCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Runs the login
        /// </summary>
        /// <param name="command">User data to login</param>
        [HttpPost("login")]
        [Produces("application/json")]
        [SwaggerResponse(StatusCodes.Status200OK, "JSON with loggedin username and tokens")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "There was a validation problem")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<ActionResult<UserLoginDto>> Login([FromBody]LoginCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        /// <summary>
        /// Runs the logout
        /// </summary>
        [HttpPost("logout")]
        [Produces("application/json")]
        [SwaggerResponse(StatusCodes.Status200OK, "Returns a successful response")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Logout()
        {
            return Ok(await Mediator.Send(new LogoutCommand()));
        }

        /// <summary>
        /// Gets the list of user roles
        /// </summary>
        [HttpGet("roles")]
        [Produces("application/json")]
        [SwaggerResponse(StatusCodes.Status200OK, "List of user roles", typeof(IEnumerable<RoleDto>))]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Roles()
        {
            return Ok(await Mediator.Send(new GetUserRolesQuery()));
        }

        /// <summary>
        /// Refresh the token
        /// </summary>
        [HttpPost("refresh")]
        [Produces("application/json")]
        [SwaggerResponse(StatusCodes.Status200OK, "JSON with tokens", typeof(RefreshTokenDto))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "There was a validation problem")]
        [SwaggerResponse(StatusCodes.Status500InternalServerError, "Internal server error")]
        public async Task<IActionResult> Refresh([FromBody]RefreshTokenCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
