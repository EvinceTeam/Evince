﻿using Evince.PassengersTransport.Application.Terminals.Queries.GetTerminalList;
using Swashbuckle.AspNetCore.Filters;
using System.Collections.Generic;

namespace Evince.API.Controllers.V1.Examples
{
    public class GetTerminalsExample : IExamplesProvider<IEnumerable<TerminalDto>>
    {
        public IEnumerable<TerminalDto> GetExamples()
        {
            return new TerminalDto[]
            {
                new TerminalDto
                {
                    Id = 1,
                    Name = "(TABE) Terminal Aldea Beleiro (Chubut) (Argentina)"
                }
            };
        }
    }
}
