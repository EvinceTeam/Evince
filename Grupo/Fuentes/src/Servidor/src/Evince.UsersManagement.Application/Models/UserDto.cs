﻿namespace Evince.UsersManagement.Application.Models
{
    public class UserDto
    {
        public string PhoneNumber { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}
