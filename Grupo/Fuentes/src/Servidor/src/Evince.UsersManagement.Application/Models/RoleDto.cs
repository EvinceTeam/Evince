﻿namespace Evince.UsersManagement.Application.Models
{
    public class RoleDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
