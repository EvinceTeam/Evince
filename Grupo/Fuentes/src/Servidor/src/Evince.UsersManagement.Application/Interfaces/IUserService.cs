﻿using Evince.UsersManagement.Application.Models;
using Evince.Application.Interfaces.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Evince.Common.Entities;

namespace Evince.UsersManagement.Application.Interfaces
{
    public interface IUserService
    {
        Task<IResult<bool>> ConfirmUserAsync(string userId, string token);
        Task<string> GenerateEmailConfirmationTokenAsync(string userId);
        Task<UserDto> GetByIdAsync(string userId);
        Task<UserDto> GetByNameAsync(string username);
        Task<IResult<string>> CreateAsync(string username, string password, string lastname, string name, Roles roleId);
        Task<IResult<bool>> AddUserToRoleByNameAsync(string username, string role);
        Task<IResult<bool>> DeleteUserAsync(string username);
        Task<IResult<IEnumerable<RoleDto>>> GetUserRolesAsync();
        Task<IResult<RoleDto>> GetUserRoleAsync(string username);
    }
}
