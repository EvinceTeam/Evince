﻿using Evince.UsersManagement.Application.Models;
using System.Threading.Tasks;

namespace Evince.UsersManagement.Application.Interfaces
{
    public interface ILoginService
    {
        Task<AuthenticationResult> LoginAsync(string username, string password, bool remember);
        Task LogoutAsync();
        Task<AuthenticationResult> RefreshTokenAsync(string token, string refreshToken);
    }
}
