﻿using MediatR;

namespace Evince.UsersManagement.Application.Users.Commands.RefreshToken
{
    public class RefreshTokenCommand : IRequest<RefreshTokenDto>
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
