﻿using MediatR;

namespace Evince.UsersManagement.Application.Users.Commands.Login
{
    public class LoginCommand : IRequest<UserLoginDto>
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Remember { get; set; }
    }
}
