﻿using MediatR;

namespace Evince.UsersManagement.Application.Users.Commands.Logout
{
    public class LogoutCommand : IRequest
    {
    }
}
