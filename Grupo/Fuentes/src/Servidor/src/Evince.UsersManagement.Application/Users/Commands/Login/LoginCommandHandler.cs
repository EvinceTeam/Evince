﻿using Evince.UsersManagement.Application.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.UsersManagement.Application.Users.Commands.Login
{
    public class LoginCommandHandler : IRequestHandler<LoginCommand, UserLoginDto>
    {
        private readonly ILoginService _loginService;
        private readonly IUserService _userService;

        public LoginCommandHandler(ILoginService loginService, IUserService userService)
        {
            _loginService = loginService;
            _userService = userService;
        }

        public async Task<UserLoginDto> Handle(LoginCommand request, CancellationToken cancellationToken)
        {
            var login = await _loginService.LoginAsync(request.Username, request.Password, request.Remember);

            if (!login.Succeeded)
            {
                // TODO: crear una excepcion particular
                throw new InvalidOperationException(login.Error);
            }

            var userRoleRequest = await _userService.GetUserRoleAsync(request.Username);

            if (!userRoleRequest.Succeeded)
            {
                // TODO: crear una excepcion particular
                throw new InvalidOperationException(userRoleRequest.Error);
            }

            return new UserLoginDto
            {
                RefreshToken = login.RefreshToken,
                Token = login.Token,
                Username = request.Username,
                Rol = userRoleRequest.Data.Id,
                RolName = userRoleRequest.Data.Name
            };
        }
    }
}
