﻿using Evince.Common.Entities;
using MediatR;

namespace Evince.UsersManagement.Application.Users.Commands.Register
{
    public class RegisterUserCommand : IRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public Roles Role { get; set; }
    }
}
