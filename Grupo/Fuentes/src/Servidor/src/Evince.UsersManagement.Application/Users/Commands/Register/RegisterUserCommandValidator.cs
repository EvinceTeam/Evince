﻿using FluentValidation;

namespace Evince.UsersManagement.Application.Users.Commands.Register
{
    public class RegisterUserCommandValidator : AbstractValidator<RegisterUserCommand>
    {
        public RegisterUserCommandValidator()
        {
            RuleFor(x => x.Username)
                .NotNull().WithMessage("El nombre de usuario no puede tener un valor nulo")
                .NotEmpty().WithMessage("El nombre de usuario no puede estar vacío");
            RuleFor(x => x.Password)
                .NotNull().WithMessage("La contraseña no puede tener un valor nulo")
                .NotEmpty().WithMessage("La contraseña no puede estar vacía")
                .MinimumLength(6).WithMessage("La contraseña debe tener al menos 6 caracteres de longitud");
            RuleFor(x => x.Lastname)
                .NotNull().WithMessage("El apellido no puede tener un valor nulo")
                .NotEmpty().WithMessage("El apellido no puede estar vacío");
            RuleFor(x => x.Name)
                .NotNull().WithMessage("El nombre no puede tener un valor nulo")
                .NotEmpty().WithMessage("El nombre no puede estar vacío");
            RuleFor(x => x.Role)
                .NotNull().WithMessage("El rol no puede tener un valor nulo")
                .NotEmpty().WithMessage("El rol no puede estar vacío");
        }
    }
}
