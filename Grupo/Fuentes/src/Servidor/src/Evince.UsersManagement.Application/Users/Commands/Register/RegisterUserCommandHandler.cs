﻿using Evince.Common.Entities;
using Evince.Common.Events;
using Evince.UsersManagement.Application.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.UsersManagement.Application.Users.Commands.Register
{
    public class RegisterUserCommandHandler : IRequestHandler<RegisterUserCommand, Unit>
    {
        private readonly IUserService _userService;
        private readonly IMediator _mediator;

        public RegisterUserCommandHandler(IUserService userService, IMediator mediator)
        {
            _userService = userService;
            _mediator = mediator;
        }

        public async Task<Unit> Handle(RegisterUserCommand request, CancellationToken cancellationToken)
        {
            var registration = await _userService.CreateAsync(
                request.Username,
                request.Password,
                request.Lastname,
                request.Name,
                request.Role);

            if (!registration.Succeeded)
            {
                // TODO: crear una excepcion particular
                throw new InvalidOperationException(registration.Error);
            }

            if (request.Role == Roles.Driver)
            {
                await _mediator.Publish(new UserWithRoleDriverCreatedNotification
                {
                    Id = registration.Data,
                    Name = request.Name,
                    Lastname = request.Lastname
                }, cancellationToken);
            } else if (request.Role == Roles.Admin || request.Role == Roles.Consultant)
            {
                await _mediator.Publish(new UserWithRoleAdminCreatedNotification
                {
                    Id = registration.Data,
                    Name = request.Name,
                    Lastname = request.Lastname
                }, cancellationToken);
            }

            return Unit.Value;
        }
    }
}
