﻿using FluentValidation;

namespace Evince.UsersManagement.Application.Users.Commands.RefreshToken
{
    public class RefreshTokenCommandValidator : AbstractValidator<RefreshTokenCommand>
    {
        public RefreshTokenCommandValidator()
        {
            RuleFor(x => x.RefreshToken)
                .NotNull().WithMessage("El refresh token no puede tener un valor nulo")
                .NotEmpty().WithMessage("El refresh token no puede estar vacío");
            RuleFor(x => x.Token)
                .NotNull().WithMessage("La token no puede tener un valor nulo")
                .NotEmpty().WithMessage("La token no puede estar vacío");
        }
    }
}
