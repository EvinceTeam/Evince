﻿namespace Evince.UsersManagement.Application.Users.Commands.RefreshToken
{
    public class RefreshTokenDto
    {
        public string RefreshToken { get; set; }
        public string Token { get; set; }
    }
}
