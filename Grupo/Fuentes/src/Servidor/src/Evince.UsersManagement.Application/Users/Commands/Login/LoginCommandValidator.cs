﻿using FluentValidation;

namespace Evince.UsersManagement.Application.Users.Commands.Login
{
    public class LoginCommandValidator : AbstractValidator<LoginCommand>
    {
        public LoginCommandValidator()
        {
            RuleFor(x => x.Username)
                .NotNull().WithMessage("El nombre de usuario no puede tener un valor nulo")
                .NotEmpty().WithMessage("El nombre de usuario no puede estar vacío");
            RuleFor(x => x.Password)
                .NotNull().WithMessage("La contraseña no puede tener un valor nulo")
                .NotEmpty().WithMessage("La contraseña no puede estar vacía")
                .MinimumLength(6).WithMessage("La contraseña debe tener al menos 6 caracteres de longitud");
        }
    }
}
