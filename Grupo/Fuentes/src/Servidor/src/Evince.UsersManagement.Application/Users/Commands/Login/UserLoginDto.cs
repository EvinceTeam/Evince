﻿namespace Evince.UsersManagement.Application.Users.Commands.Login
{
    public class UserLoginDto
    {
        public string Username { get; set; }
        public string Token { get; set; }
        public string RefreshToken { get; set; }
        public int Rol { get; set; }
        public string RolName { get; set; }
    }
}
