﻿using Evince.UsersManagement.Application.Interfaces;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.UsersManagement.Application.Users.Commands.RefreshToken
{
    public class RefreshTokenCommandHandler : IRequestHandler<RefreshTokenCommand, RefreshTokenDto>
    {
        private readonly ILoginService _loginService;

        public RefreshTokenCommandHandler(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public async Task<RefreshTokenDto> Handle(RefreshTokenCommand command, CancellationToken cancellationToken)
        {
            var authResponse = await _loginService.RefreshTokenAsync(command.Token, command.RefreshToken);

            if (!authResponse.Succeeded)
            {
                throw new InvalidOperationException(authResponse.Error);
            }

            return new RefreshTokenDto
            {
                RefreshToken = authResponse.RefreshToken,
                Token = authResponse.Token
            };
        }
    }
}
