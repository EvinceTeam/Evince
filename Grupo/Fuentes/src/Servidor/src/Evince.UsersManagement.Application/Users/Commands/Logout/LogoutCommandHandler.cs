﻿using Evince.UsersManagement.Application.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.UsersManagement.Application.Users.Commands.Logout
{
    public class LogoutCommandHandler : IRequestHandler<LogoutCommand, Unit>
    {
        private readonly ILoginService _loginService;

        public LogoutCommandHandler(ILoginService loginService)
        {
            _loginService = loginService;
        }

        public async Task<Unit> Handle(LogoutCommand request, CancellationToken cancellationToken)
        {
            await _loginService.LogoutAsync();

            return Unit.Value;
        }
    }
}
