﻿using Evince.UsersManagement.Application.Models;
using MediatR;
using System.Collections.Generic;

namespace Evince.UsersManagement.Application.Users.Queries.GetRoles
{
    public class GetUserRolesQuery : IRequest<IEnumerable<RoleDto>>
    {
    }
}
