﻿using Evince.UsersManagement.Application.Models;
using Evince.UsersManagement.Application.Interfaces;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.UsersManagement.Application.Users.Queries.GetRoles
{
    public class GetUserRolesQueryHandler : IRequestHandler<GetUserRolesQuery, IEnumerable<RoleDto>>
    {
        private readonly IUserService _userService;
        private readonly IMemoryCache _cache;

        public GetUserRolesQueryHandler(IUserService userService, IMemoryCache cache)
        {
            _userService = userService;
            _cache = cache;
        }

        public async Task<IEnumerable<RoleDto>> Handle(GetUserRolesQuery request, CancellationToken cancellationToken)
        {
            var cachedRoles = await _cache.GetOrCreateAsync("Query-GetUserRoles", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600);

                return _userService.GetUserRolesAsync();
            });

            return cachedRoles.Data;
        }
    }
}
