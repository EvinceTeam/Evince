﻿using Evince.Common.Entities;
using Evince.Common.Guards;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class Driver : BaseEntity<string>
    {
        public Driver(string id, string name, string lastname)
        {
            ValidateInvariants(id, name, lastname);

            Id = id;
            Name = name;
            Lastname = lastname;
        }

        public string Name { get; private set; }
        public string Lastname { get; private set; }

        public string Fullname { get { return $"{Lastname} {Name}"; } }

        private void ValidateInvariants(string id, string name, string lastname)
        {
            Guard.Against.NullOrWhiteSpace(id, nameof(id));
            Guard.Against.NullOrWhiteSpace(name, nameof(name));
            Guard.Against.NullOrWhiteSpace(lastname, nameof(lastname));
        }
    }
}
