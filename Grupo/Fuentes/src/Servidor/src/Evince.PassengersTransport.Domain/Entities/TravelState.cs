﻿namespace Evince.PassengersTransport.Domain.Entities
{
    public enum TravelState
    {
        Pending,
        InProgress,
        Finalized,
    }
}
