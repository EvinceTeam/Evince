﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using System;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class Delay : BaseEntity<int>
    {
        public Delay(TimeSpan time, string description, DelayType type = DelayType.Undefined)
        {
            ValidateInvariants(time, description);

            Type = type;
            Time = time;
            Description = description;
        }

        public DelayType Type { get; private set; }
        public TimeSpan Time { get; private set; }
        public string Description { get; private set; }

        private void ValidateInvariants(TimeSpan time, string description)
        {
            Guard.Against.Null(time, nameof(time));
            Guard.Against.Null(description, nameof(description));
        }
    }
}
