﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class DriverAssignment : BaseEntity<int>
    {
        public DriverAssignment(Driver driver)
        {
            ValidateInvariants(driver);

            Driver = driver;
            DriverId = driver.Id;
        }

        protected DriverAssignment() { }

        public string DriverId { get; private set; }
        public Driver Driver { get; private set; }
        public IList<Assignment> Assignments { get; private set; } = new List<Assignment>();

        public DriverAssignment AddAssignment(Assignment assignment)
        {
            Guard.Against.Null(assignment, nameof(assignment));

            var driverDayAssigments = Assignments.Where(a => a.DayOfWeek == assignment.DayOfWeek);

            foreach (var driverDayAssigment in driverDayAssigments)
            {
                if (HasConflictingDatesBetweenAssignment(driverDayAssigment, assignment))
                {
                    throw new InvalidOperationException("Ya existe una asignación para el día y horario especificado");
                }
            }

            Assignments.Add(assignment);

            return this;
        }

        public bool HasConflictingDatesBetweenAssignment(Assignment existingAssigment, Assignment newAssigment)
        {
            Guard.Against.Null(existingAssigment, nameof(existingAssigment));
            Guard.Against.Null(newAssigment, nameof(newAssigment));

            if (existingAssigment.StartTime <= newAssigment.StartTime && existingAssigment.ApproximatedEndTime >= newAssigment.ApproximatedEndTime)
            {
                return true;
            }

            if (existingAssigment.StartTime >= newAssigment.StartTime && existingAssigment.ApproximatedEndTime <= newAssigment.ApproximatedEndTime)
            {
                return true;
            }

            if (existingAssigment.StartTime >= newAssigment.StartTime &&
                existingAssigment.StartTime <= newAssigment.ApproximatedEndTime &&
                existingAssigment.ApproximatedEndTime >= newAssigment.ApproximatedEndTime)
            {
                return true;
            }

            if (existingAssigment.StartTime <= newAssigment.StartTime &&
                existingAssigment.ApproximatedEndTime >= newAssigment.StartTime &&
                existingAssigment.ApproximatedEndTime <= newAssigment.ApproximatedEndTime)
            {
                return true;
            }

            return false;
        }

        private void ValidateInvariants(Driver driver)
        {
            Guard.Against.Null(driver, nameof(driver));
        }
    }
}
