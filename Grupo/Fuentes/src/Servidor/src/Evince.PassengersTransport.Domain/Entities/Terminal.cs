﻿using Evince.Common.Entities;
using Evince.Common.Guards;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class Terminal : BaseEntity<int>
    {
        public Terminal(int id, string description)
        {
            ValidateInvariants(id, description);

            Id = id;
            Description = description;
        }

        public string Description { get; private set; }

        public string Code { get { return $"{Description.Split('(')[1].Split(')')[0].Trim()}"; } }
        public string Name { get { return $"{Description.Split('(')[1].Split(')')[1].Trim()}"; } }
        public string Province { get { return $"{Description.Split('(')[2].Split(')')[0].Trim()}"; } }
        public string Country { get { return $"{Description.Split('(')[3].Split(')')[0].Trim()}"; } }

        public string ShortDescription { get { return $"({Code}) {Name}"; } }

        private void ValidateInvariants(int id, string description)
        {
            Guard.Against.OutOfRange(id, nameof(id), 1, int.MaxValue);
            Guard.Against.NullOrWhiteSpace(description, nameof(description));
        }
    }
}
