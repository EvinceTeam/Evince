﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using System;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class Journey : BaseEntity<int>
    {
        public Journey(Terminal startNode, Terminal endNode, TimeSpan duration)
        {
            ValidateInvariants(startNode, endNode, duration);

            StartNode = startNode;
            StartNodeId = startNode.Id;
            EndNode = endNode;
            EndNodeId = endNode.Id;
            Duration = duration;
        }

        protected Journey()
        {
        }

        public int StartNodeId { get; private set; }
        public Terminal StartNode { get; private set; }
        public int EndNodeId { get; private set; }
        public Terminal EndNode { get; private set; }
        public TimeSpan Duration { get; private set; }

        public string Description
        {
            get
            {
                return $"{StartNode.ShortDescription} - {EndNode.ShortDescription}";
            }
        }

        public string StartNodeDescription
        {
            get
            {
                return StartNode.Description;
            }
        }

        public string EndNodeDescription
        {
            get
            {
                return EndNode.Description;
            }
        }

        private void ValidateInvariants(Terminal startNode, Terminal endNode, TimeSpan duration)
        {
            Guard.Against.Null(startNode, nameof(startNode));
            Guard.Against.Null(endNode, nameof(endNode));
            Guard.Against.Null(duration, nameof(duration));

            if (startNode.Id == endNode.Id)
            {
                throw new ArgumentException("Las terminales no pueden ser las mismas");
            }
        }

        #region operators
        public static bool operator !=(Journey a, Journey b)
        {
            return !(a == b);
        }

        public static bool operator ==(Journey a, Journey b)
        {
            if (a is null && b is null) return true;

            if (a is null || b is null) return false;

            return a.Equals(b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Journey journey)) return false;

            if (GetType() != obj.GetType())
                return false;

            return
                (StartNodeId == journey.StartNodeId && EndNodeId == journey.EndNodeId) ||
                (EndNodeId == journey.StartNodeId && StartNodeId == journey.EndNodeId);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                // Choose large primes to avoid hashing collisions
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;

                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, Id) ? Id.GetHashCode() : 0);
                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, StartNode) ? StartNode.GetHashCode() : 0);
                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, EndNode) ? EndNode.GetHashCode() : 0);
                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, Duration) ? Duration.GetHashCode() : 0);

                return hash;
            }
        }
        #endregion
    }
}
