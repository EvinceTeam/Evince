﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using System;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class Route : BaseEntity<int>
    {
        public Route(RoadMap roadmap)
        {
            ValidateInvariants(roadmap);

            RoadMap = roadmap;
        }

        protected Route() { }

        public int RoadMapId { get; private set; }
        public RoadMap RoadMap { get; private set; }
        public IList<Travel> Travels { get; private set; }

        private void ValidateInvariants(RoadMap roadmap)
        {
            Guard.Against.Null(roadmap, nameof(roadmap));
        }
    }
}
