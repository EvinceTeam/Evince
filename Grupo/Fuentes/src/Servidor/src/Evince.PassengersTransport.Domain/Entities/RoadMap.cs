﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class RoadMap : BaseEntity<int>
    {
        public IList<RoadMapItem> Items { get; private set; }

        public RoadMap()
        {
            Items = new List<RoadMapItem>();
        }

        public string From
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var firstItem = Items.OrderBy(i => i.Order).FirstOrDefault();

                return firstItem?.Journey?.StartNodeDescription;
            }
        }

        public string FromShort
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var firstItem = Items.OrderBy(i => i.Order).FirstOrDefault();

                return firstItem?.Journey?.StartNode?.Name;
            }
        }

        public string FromCode
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var firstItem = Items.OrderBy(i => i.Order).FirstOrDefault();

                return firstItem?.Journey?.StartNode?.Code;
            }
        }

        public string FromProvince
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var firstItem = Items.OrderBy(i => i.Order).FirstOrDefault();

                return firstItem?.Journey?.StartNode?.Province;
            }
        }

        public string FromCountry
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var firstItem = Items.OrderBy(i => i.Order).FirstOrDefault();

                return firstItem?.Journey?.StartNode?.Country;
            }
        }

        public int? FromId
        {
            get
            {
                if (Items.Count == 0) return 0;

                var firstItem = Items.OrderBy(i => i.Order).FirstOrDefault();

                return firstItem?.Journey?.StartNode?.Id;
            }
        }

        public string To
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var lastItem = Items.OrderBy(i => i.Order).LastOrDefault();

                return lastItem?.Journey?.EndNodeDescription;
            }
        }

        public string ToShort
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var lastItem = Items.OrderBy(i => i.Order).LastOrDefault();

                return lastItem?.Journey?.EndNode?.Name;
            }
        }

        public string ToCode
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var lastItem = Items.OrderBy(i => i.Order).LastOrDefault();

                return lastItem?.Journey?.EndNode?.Code;
            }
        }

        public string ToProvince
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var lastItem = Items.OrderBy(i => i.Order).LastOrDefault();

                return lastItem?.Journey?.EndNode?.Province;
            }
        }

        public string ToCountry
        {
            get
            {
                if (Items.Count == 0) return string.Empty;

                var lastItem = Items.OrderBy(i => i.Order).LastOrDefault();

                return lastItem?.Journey?.EndNode?.Country;
            }
        }

        public int? ToId
        {
            get
            {
                if (Items.Count == 0) return 0;

                var lastItem = Items.OrderBy(i => i.Order).LastOrDefault();

                return lastItem?.Journey?.EndNode?.Id;
            }
        }

        public TimeSpan ApproximateDuration
        {
            get
            {
                if (Items.Count == 0) return TimeSpan.Zero;

                var journeysDuration = Items.Sum(i => i.Journey.Duration.TotalMinutes);

                foreach (var item in Items)
                {
                    journeysDuration += 15;
                }

                return TimeSpan.FromMinutes(journeysDuration);
            }
        }

        public RoadMap AddRouteItem(Journey journey)
        {
            Guard.Against.Null(journey, nameof(journey));

            var order = Items.Count;

            var item = new RoadMapItem(order, journey);

            return AddRouteItem(item);
        }

        public RoadMap AddRouteItem(RoadMapItem newItem)
        {
            Guard.Against.Null(newItem, nameof(newItem));

            if (Items.Any(i => i.Order == newItem.Order))
            {
                throw new InvalidOperationException("Ya existe un trayecto en la posición que está intentando agregar");
            }

            if (Items.Any(i => i.Journey == newItem.Journey))
            {
                throw new InvalidOperationException("El trayecto que intenta agregar ya existe en el recorrido");
            }

            Items.Add(newItem);

            Items = GenerateOrderedRoadMapItems();

            return this;
        }

        public RoadMap RemoveRouteItem(Journey journey)
        {
            Guard.Against.Null(journey, nameof(journey));

            var existingItem = Items.FirstOrDefault(i => i.Journey == journey);

            if (existingItem == null)
            {
                throw new InvalidOperationException("El trayecto que intenta eliminar no existe");
            }

            return RemoveRouteItem(existingItem);
        }

        public RoadMap RemoveRouteItem(RoadMapItem item)
        {
            Guard.Against.Null(item, nameof(item));

            var existingItem = Items.FirstOrDefault(i => i.Journey == item.Journey);

            if (existingItem == null)
            {
                throw new InvalidOperationException("El trayecto que intenta eliminar no existe");
            }

            Items.Remove(existingItem);

            Items = GenerateOrderedRoadMapItems();

            return this;
        }

        private IList<RoadMapItem> GenerateOrderedRoadMapItems()
        {
            var orderedList = new List<RoadMapItem>();

            for (int i = 0; i < Items.Count; i++)
            {
                orderedList.Add(new RoadMapItem(i, Items[i].Journey));
            }

            return orderedList;
        }
    }
}
