﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using System;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class Bus : BaseEntity<string>
    {
        public Bus(string id, int capacity)
        {
            ValidateInvariants(id, capacity);

            Id = id;
            Capacity = capacity;
        }

        public int Capacity { get; private set; }

        private void ValidateInvariants(string id, int capacity)
        {
            Guard.Against.NullOrWhiteSpace(id, nameof(id));
            Guard.Against.OutOfRange(capacity, nameof(capacity), 1, int.MaxValue);
        }
    }
}
