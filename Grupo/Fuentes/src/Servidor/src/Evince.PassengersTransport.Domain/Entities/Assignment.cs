﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using System;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class Assignment : BaseEntity<int>
    {
        public Assignment(RoadMap roadmap, DayOfWeek dayOfWeek, TimeSpan startTime)
        {
            ValidateInvariants(roadmap, dayOfWeek, startTime);

            RoadMap = roadmap;
            RoadMapId = roadmap.Id;
            DayOfWeek = dayOfWeek;
            StartTime = startTime;
        }

        protected Assignment() { }

        public int RoadMapId { get; private set; }
        public RoadMap RoadMap { get; private set; }
        public DayOfWeek DayOfWeek { get; private set; }
        public TimeSpan StartTime { get; private set; }
        public TimeSpan ApproximatedEndTime
        {
            get
            {
                return StartTime.Add(RoadMap.ApproximateDuration);
            }
        }

        private void ValidateInvariants(RoadMap roadmap, DayOfWeek dayOfWeek, TimeSpan startTime)
        {
            Guard.Against.Null(roadmap, nameof(roadmap));
            Guard.Against.Null(dayOfWeek, nameof(dayOfWeek));
            Guard.Against.Null(startTime, nameof(startTime));
        }
    }
}
