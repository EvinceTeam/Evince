﻿namespace Evince.PassengersTransport.Domain.Entities
{
    public enum DelayType
    {
        RouteCut,
        VehicleFailure,
        Accident,
        DriverDisability,
        Undefined
    }

    public static class DelayTypeExtensions
    {
        public static string ToFriendlyString(this DelayType dt)
        {
            switch (dt)
            {
                case DelayType.RouteCut:
                    return "Corte de ruta";
                case DelayType.VehicleFailure:
                    return "Falla en el vehículo";
                case DelayType.Accident:
                    return "Accidente";
                case DelayType.DriverDisability:
                    return "Incapacidad del chofer";
                case DelayType.Undefined:
                    return "Indefinido";
                default:
                    return string.Empty;
            }
        }
    }
}
