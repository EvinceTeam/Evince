﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class Travel : BaseEntity<int>
    {
        public Travel(
            DateTime stipulatedStartTime,
            DateTime? startTime,
            DateTime? endTime,
            int numberOfPassengers,
            RoadMap roadmap,
            Journey journey,
            Driver driver,
            Bus bus,
            Route route
            )
        {
            ValidateInvariants(
                stipulatedStartTime,
                numberOfPassengers,
                roadmap,
                journey,
                driver,
                bus,
                route
                );

            StipulatedStartTime = stipulatedStartTime;
            StartTime = startTime;
            EndTime = endTime;
            State = TravelState.Pending;
            NumberOfPassengers = numberOfPassengers;
            RoadMap = roadmap;
            Journey = journey;
            DriverId = driver.Id;
            DriverFullname = driver.Fullname;
            BusId = bus?.Id;
            Delays = new List<Delay>();
            Route = route;
        }

        protected Travel() { }

        public DateTime StipulatedStartTime { get; private set; }
        public DateTime StipulatedEndTime
        {
            get
            {
                return StipulatedStartTime.Add(Journey.Duration);
            }
        }
        public DateTime? StartTime { get; private set; }
        public DateTime? EndTime { get; private set; }
        public TravelState State { get; private set; }
        public int NumberOfPassengers { get; set; }
        public int RoadMapId { get; private set; }
        public RoadMap RoadMap { get; private set; }
        public int JourneyId { get; private set; }
        public Journey Journey { get; private set; }
        public string DriverId { get; private set; }
        public Driver Driver { get; set; }
        public string DriverFullname { get; private set; }
        public string BusId { get; private set; }
        public Bus Bus { get; private set; }
        public int RouteId { get; set; }
        public Route Route { get; set; }
        public IList<Delay> Delays { get; private set; }

        public string Name { get { return $"{Journey.Description} {StipulatedStartTime.ToString("HH:mm")}"; } }

        public string FromCode
        {
            get
            {
                return Journey?.StartNode?.Code;
            }
        }

        public string ToCode
        {
            get
            {
                return Journey?.EndNode?.Code;
            }
        }

        public RoadMapItem RoadmapTravel
        {
            get
            {
                return RoadMap.Items.FirstOrDefault(i => i.Journey == Journey);
            }
        }

        private void ValidateInvariants(
            DateTime stipulatedStartTime,
            int numberOfPassengers,
            RoadMap roadmap,
            Journey journey,
            Driver driver,
            Bus bus,
            Route route
            )
        {
            Guard.Against.Null(stipulatedStartTime, nameof(stipulatedStartTime));
            Guard.Against.OutOfRange(numberOfPassengers, nameof(numberOfPassengers), 0, int.MaxValue);
            Guard.Against.Null(roadmap, nameof(roadmap));
            Guard.Against.Null(journey, nameof(journey));
            Guard.Against.Null(driver, nameof(driver));
            Guard.Against.Null(bus, nameof(bus));
            Guard.Against.Null(route, nameof(route));

            if (stipulatedStartTime < DateTime.Now)
            {
                throw new ArgumentException("La fecha estipulada de salida no puede ser menor a hoy");
            }

            if (numberOfPassengers > bus.Capacity)
            {
                throw new ArgumentException("El número de pasajeros excede la capacidad del colectivo asignado");
            }
        }

        public Travel MarkTravelStart()
        {
            if (State != TravelState.Pending)
            {
                throw new InvalidOperationException("El viaje debe tener el estado 'Pendiente' para poder iniciar");
            }

            StartTime = DateTime.Now;
            State = TravelState.InProgress;

            return this;
        }

        public Travel MarkTravelEnd()
        {
            if (State != TravelState.InProgress)
            {
                throw new InvalidOperationException("El viaje debe tener el estado 'En progreso' para poder finalizar");
            }

            EndTime = DateTime.Now;
            State = TravelState.Finalized;

            return this;
        }

        public Travel ReplaceDriver(Driver driver)
        {
            if (State != TravelState.Pending)
            {
                throw new InvalidOperationException("El viaje debe tener el estado 'Pendiente' para poder modificar el chofer");
            }

            Guard.Against.Null(driver, nameof(driver));

            DriverId = driver.Id;
            DriverFullname = driver.Fullname;

            return this;
        }

        public Travel ReplaceBus(Bus bus)
        {
            Guard.Against.Null(bus, nameof(bus));

            BusId = bus.Id;

            return this;
        }

        public bool HasConflictingDatesWithTravel(Travel travel)
        {
            Guard.Against.Null(travel, nameof(travel));

            if (StipulatedStartTime < travel.StipulatedStartTime && StipulatedEndTime > travel.StipulatedEndTime)
            {
                return true;
            }

            if (StipulatedStartTime > travel.StipulatedStartTime && StipulatedEndTime < travel.StipulatedEndTime)
            {
                return true;
            }

            if (StipulatedStartTime > travel.StipulatedStartTime &&
                StipulatedStartTime < travel.StipulatedEndTime &&
                StipulatedEndTime > travel.StipulatedEndTime)
            {
                return true;
            }

            if (StipulatedStartTime < travel.StipulatedStartTime &&
                StipulatedEndTime > travel.StipulatedStartTime &&
                StipulatedEndTime < travel.StipulatedEndTime)
            {
                return true;
            }

            return false;
        }

        public Travel AddDelay(Delay delay)
        {
            Guard.Against.Null(delay, nameof(delay));

            Delays.Add(delay);

            return this;
        }

        #region operators
        public static bool operator !=(Travel a, Travel b)
        {
            return !(a == b);
        }

        public static bool operator ==(Travel a, Travel b)
        {
            if (a is null && b is null) return true;

            if (a is null || b is null) return false;

            return a.Equals(b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Travel travel)) return false;

            if (GetType() != obj.GetType())
                return false;

            return
                StipulatedStartTime == travel.StipulatedStartTime &&
                Journey == travel.Journey &&
                RoadMap.Id == travel.RoadMap.Id;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                // Choose large primes to avoid hashing collisions
                const int HashingBase = (int)2166136261;
                const int HashingMultiplier = 16777619;

                int hash = HashingBase;

                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, Id) ? Id.GetHashCode() : 0);
                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, StipulatedStartTime) ? StipulatedStartTime.GetHashCode() : 0);
                hash = (hash * HashingMultiplier) ^ (!ReferenceEquals(null, NumberOfPassengers) ? NumberOfPassengers.GetHashCode() : 0);

                return hash;
            }
        }
        #endregion
    }
}
