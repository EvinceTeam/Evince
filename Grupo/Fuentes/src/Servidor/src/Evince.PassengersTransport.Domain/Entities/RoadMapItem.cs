﻿using Evince.Common.Entities;
using Evince.Common.Guards;

namespace Evince.PassengersTransport.Domain.Entities
{
    public class RoadMapItem : BaseEntity<int>
    {
        public RoadMapItem(int order, Journey journey)
        {
            ValidateInvariants(order, journey);

            Order = order;
            Journey = journey;
        }

        protected RoadMapItem() { }

        public int Order { get; private set; }
        public int JourneyId { get; private set; }
        public Journey Journey { get; private set; }

        private void ValidateInvariants(int order, Journey journey)
        {
            Guard.Against.OutOfRange(order, nameof(order), 0, int.MaxValue);
            Guard.Against.Null(journey, nameof(journey));
        }
    }
}
