﻿using Evince.Administration.Domain.Entities;
using Evince.PassengersTransport.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Application.Interfaces.Persistance
{
    public interface IEvinceDbContext
    {
        DbSet<Terminal> Terminals { get; }
        DbSet<Bus> Buses { get; }
        DbSet<Driver> Drivers { get; }
        DbSet<Travel> Travels { get; }
        DbSet<RoadMap> RoadMaps { get; }
        DbSet<Journey> Journeys { get; }
        DbSet<Delay> Delays { get; }
        DbSet<Route> Routes { get; }
        DbSet<Assignment> Assignments { get; }
        DbSet<DriverAssignment> DriverAssignments { get; }

        DbSet<Administrator> Administrators { get; }
        DbSet<Subscription> Subscriptions { get; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        int SaveChanges();
    }
}
