﻿using Evince.Administration.Domain.Entities;

namespace Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories
{
    public interface ISubscriptionReadOnlyRepository : IEfReadOnlyRepository<Subscription, int>
    {
    }
}
