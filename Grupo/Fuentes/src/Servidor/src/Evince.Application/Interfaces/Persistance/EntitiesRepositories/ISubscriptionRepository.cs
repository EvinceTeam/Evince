﻿using Evince.Administration.Domain.Entities;

namespace Evince.Application.Interfaces.Persistance.EntitiesRepositories
{
    public interface ISubscriptionRepository : IEfRepository<Subscription, int>
    {
    }
}
