﻿using Evince.Administration.Domain.Entities;

namespace Evince.Application.Interfaces.Persistance.EntitiesRepositories
{
    public interface IAdministratorRepository : IEfRepository<Administrator, string>
    {
    }
}
