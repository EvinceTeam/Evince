﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Interfaces.Persistance.EntitiesRepositories
{
    public interface IDelayRepository : IEfRepository<Delay, int>
    {
    }
}
