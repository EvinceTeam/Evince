﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories
{
    public interface IRouteReadOnlyRepository : IEfReadOnlyRepository<Route, int>
    {
    }
}
