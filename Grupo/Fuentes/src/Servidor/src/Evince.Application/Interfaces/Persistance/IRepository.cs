﻿using Evince.Common.Entities;
using System.Collections.Generic;

namespace Evince.Application.Interfaces.Persistance
{
    public interface IRepository<T, TKey> where T : BaseEntity<TKey>
    {
        T GetById(TKey id);
        T GetSingleBySpec(ISpecification<T> spec);
        IEnumerable<T> ListAll();
        IEnumerable<T> List(ISpecification<T> spec);
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        int Count(ISpecification<T> spec);
        void SaveChanges();
    }
}
