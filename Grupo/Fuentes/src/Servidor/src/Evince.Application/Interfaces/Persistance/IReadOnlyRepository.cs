﻿using Evince.Common.Entities;
using System.Collections.Generic;

namespace Evince.Application.Interfaces.Persistance
{
    public interface IReadOnlyRepository<T, TKey> where T : BaseEntity<TKey>
    {
        T GetById(TKey id);
        T GetSingleBySpec(ISpecification<T> spec);
        IEnumerable<T> ListAll();
        IEnumerable<T> List(ISpecification<T> spec);
        int Count(ISpecification<T> spec);
    }
}
