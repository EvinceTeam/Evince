﻿using Evince.Administration.Domain.Entities;
using Evince.PassengersTransport.Domain.Entities;
using System.Linq;

namespace Evince.Application.Interfaces.Persistance
{
    public interface IReadOnlyEvinceDbContext
    {
        IQueryable<Terminal> Terminals { get; }
        IQueryable<Driver> Drivers { get; }
        IQueryable<Bus> Buses { get; }
        IQueryable<Travel> Travels { get; }
        IQueryable<RoadMap> RoadMaps { get; }
        IQueryable<Journey> Journeys { get; }
        IQueryable<Subscription> Subscriptions { get; }
    }
}
