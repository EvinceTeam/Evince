﻿using Evince.Common.Entities;

namespace Evince.Application.Interfaces.Persistance
{
    public interface IEfRepository<T, TKey> : IAsyncRepository<T, TKey>, IRepository<T, TKey> where T : BaseEntity<TKey>
    {
    }
}
