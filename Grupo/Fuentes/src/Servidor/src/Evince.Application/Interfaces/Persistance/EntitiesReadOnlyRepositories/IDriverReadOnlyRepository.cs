﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories
{
    public interface IDriverReadOnlyRepository : IEfReadOnlyRepository<Driver, string>
    {
    }
}
