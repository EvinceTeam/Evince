﻿using Evince.Common.Entities;

namespace Evince.Application.Interfaces.Persistance
{
    public interface IEfReadOnlyRepository<T, TKey> : IAsyncReadOnlyRepository<T, TKey>, IReadOnlyRepository<T, TKey> where T : BaseEntity<TKey>
    {
    }
}
