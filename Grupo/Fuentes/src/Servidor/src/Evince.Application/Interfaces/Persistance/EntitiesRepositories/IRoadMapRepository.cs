﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Interfaces.Persistance.EntitiesRepositories
{
    public interface IRoadMapRepository : IEfRepository<RoadMap, int>
    {
    }
}
