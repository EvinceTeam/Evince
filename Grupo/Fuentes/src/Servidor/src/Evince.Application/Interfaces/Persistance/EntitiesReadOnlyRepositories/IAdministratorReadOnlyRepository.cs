﻿using Evince.Administration.Domain.Entities;

namespace Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories
{
    public interface IAdministratorReadOnlyRepository : IEfReadOnlyRepository<Administrator, string>
    {
    }
}
