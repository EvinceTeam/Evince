﻿namespace Evince.Application.Interfaces.Models
{
    public interface IResult<T>
    {
        bool Succeeded { get; }
        T Data { get; }
        string Error { get; }
    }
}
