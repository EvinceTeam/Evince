﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Specifications
{
    public class DriverAssignmentWithAssignmentsSpecification : BaseSpecification<DriverAssignment>
    {
        public DriverAssignmentWithAssignmentsSpecification(string driver) : base(da => da.DriverId == driver)
        {
            AddInclude(da => da.Driver);
            AddInclude(da => da.Assignments);
            AddInclude("Assignments.RoadMap");
            AddInclude("Assignments.RoadMap.Items");
            AddInclude("Assignments.RoadMap.Items.Journey");
        }
    }
}
