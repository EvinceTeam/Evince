﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Specifications
{
    public class RoadMapWithJourneysSpecification : BaseSpecification<RoadMap>
    {
        public RoadMapWithJourneysSpecification() : base(null)
        {
            AddInclude(r => r.Items);
            AddInclude("Items.Journey");
        }
    }
}
