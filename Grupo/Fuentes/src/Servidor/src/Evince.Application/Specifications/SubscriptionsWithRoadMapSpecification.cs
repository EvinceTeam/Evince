﻿using Evince.Administration.Domain.Entities;

namespace Evince.Application.Specifications
{
    public class SubscriptionsWithRoadMapSpecification : BaseSpecification<Subscription>
    {
        public SubscriptionsWithRoadMapSpecification(string administratorId) : base(s => s.AdministratorId == administratorId)
        {
            AddInclude(s => s.RoadMap);
        }
    }
}
