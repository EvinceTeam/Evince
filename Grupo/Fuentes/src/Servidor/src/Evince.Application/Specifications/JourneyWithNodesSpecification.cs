﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Specifications
{
    public class JourneyWithNodesSpecification : BaseSpecification<Journey>
    {
        public JourneyWithNodesSpecification() : base(null)
        {
            AddInclude(j => j.StartNode);
            AddInclude(j => j.EndNode);
        }

        public JourneyWithNodesSpecification(int startNode, int endNode) : base(j => j.StartNode.Id == startNode && j.EndNode.Id == endNode)
        {
            AddInclude(j => j.StartNode);
            AddInclude(j => j.EndNode);
        }
    }
}
