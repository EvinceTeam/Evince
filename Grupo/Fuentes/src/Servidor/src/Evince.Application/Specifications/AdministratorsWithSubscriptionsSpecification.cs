﻿using Evince.Administration.Domain.Entities;

namespace Evince.Application.Specifications
{
    public class AdministratorsWithSubscriptionsSpecification : BaseSpecification<Administrator>
    {
        public AdministratorsWithSubscriptionsSpecification(string administratorId) : base(administrator => administrator.Id == administratorId)
        {
            AddInclude(a => a.Subscriptions);
            AddInclude("Subscriptions.RoadMap");
        }
    }
}
