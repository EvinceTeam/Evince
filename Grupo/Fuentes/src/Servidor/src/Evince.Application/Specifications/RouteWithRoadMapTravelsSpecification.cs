﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Specifications
{
    public class RouteWithRoadMapTravelsSpecification : BaseSpecification<Route>
    {
        public RouteWithRoadMapTravelsSpecification(int id) : base(r => r.Id == id)
        {
            AddInclude(r => r.RoadMap);
            AddInclude(r => r.Travels);
            AddInclude("Travels.RoadMap");
            AddInclude("Travels.Bus");
            AddInclude("Travels.Delays");
            AddInclude("Travels.Journey");
            AddInclude("Travels.Journey.StartNode");
            AddInclude("Travels.Journey.EndNode");
            AddInclude("Travels.RoadMap.Items");
            AddInclude("Travels.RoadMap.Items.Journey");
            AddInclude("Travels.RoadMap.Items.Journey.StartNode");
            AddInclude("Travels.RoadMap.Items.Journey.EndNode");
        }
    }
}
