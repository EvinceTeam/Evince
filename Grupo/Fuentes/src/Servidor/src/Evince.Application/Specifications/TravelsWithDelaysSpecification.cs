﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Application.Specifications
{
    public class TravelsWithDelaysSpecification : BaseSpecification<Travel>
    {
        public TravelsWithDelaysSpecification(int travelId) : base(t => t.Id == travelId)
        {
            AddInclude(t => t.Delays);
        }
    }
}
