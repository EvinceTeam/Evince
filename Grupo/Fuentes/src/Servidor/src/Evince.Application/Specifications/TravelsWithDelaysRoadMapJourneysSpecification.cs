﻿using Evince.PassengersTransport.Domain.Entities;
using System;

namespace Evince.Application.Specifications
{
    public class TravelsWithDelaysRoadMapJourneysSpecification : BaseSpecification<Travel>
    {
        public TravelsWithDelaysRoadMapJourneysSpecification() : base(null)
        {
            AddInclude(travel => travel.Bus);
            AddInclude(travel => travel.Delays);
            AddInclude(travel => travel.RoadMap);
            AddInclude(travel => travel.Journey);
            AddInclude("Journey.StartNode");
            AddInclude("Journey.EndNode");
            AddInclude("RoadMap.Items");
            AddInclude("RoadMap.Items.Journey");
            AddInclude("RoadMap.Items.Journey.StartNode");
            AddInclude("RoadMap.Items.Journey.EndNode");
        }

        public TravelsWithDelaysRoadMapJourneysSpecification(int travelId) : base(t => t.Id == travelId)
        {
            AddInclude(travel => travel.Bus);
            AddInclude(travel => travel.Delays);
            AddInclude(travel => travel.RoadMap);
            AddInclude(travel => travel.Journey);
            AddInclude("Journey.StartNode");
            AddInclude("Journey.EndNode");
            AddInclude("RoadMap.Items");
            AddInclude("RoadMap.Items.Journey");
            AddInclude("RoadMap.Items.Journey.StartNode");
            AddInclude("RoadMap.Items.Journey.EndNode");
        }

        public TravelsWithDelaysRoadMapJourneysSpecification(int roadMapId, DateTime fromDate) : base(t => t.RoadMap.Id == roadMapId && t.StipulatedStartTime >= fromDate)
        {
            AddInclude(travel => travel.Bus);
            AddInclude(travel => travel.Delays);
            AddInclude(travel => travel.RoadMap);
            AddInclude(travel => travel.Journey);
            AddInclude("Journey.StartNode");
            AddInclude("Journey.EndNode");
            AddInclude("RoadMap.Items");
            AddInclude("RoadMap.Items.Journey");
            AddInclude("RoadMap.Items.Journey.StartNode");
            AddInclude("RoadMap.Items.Journey.EndNode");
        }

        public TravelsWithDelaysRoadMapJourneysSpecification(DateTime fromDate) : base(t => t.StipulatedStartTime >= fromDate)
        {
            AddInclude(travel => travel.Bus);
            AddInclude(travel => travel.Delays);
            AddInclude(travel => travel.RoadMap);
            AddInclude(travel => travel.Journey);
            AddInclude("Journey.StartNode");
            AddInclude("Journey.EndNode");
            AddInclude("RoadMap.Items");
            AddInclude("RoadMap.Items.Journey");
            AddInclude("RoadMap.Items.Journey.StartNode");
            AddInclude("RoadMap.Items.Journey.EndNode");
        }
    }
}
