﻿using MediatR;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Application.Infrastructure.Behaviors
{
    public class RequestPerformanceBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly Stopwatch _timer;
        private readonly ILogger<TRequest> _logger;

        public RequestPerformanceBehavior(ILogger<TRequest> logger)
        {
            _timer = new Stopwatch();
            _logger = logger;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            _timer.Start();

            var response = await next();

            _timer.Stop();

            var requestName = typeof(TRequest).Name;

            if (_timer.ElapsedMilliseconds > 500)
            {
                _logger.LogWarning($"Evince Long Running Request: {requestName} ({_timer.ElapsedMilliseconds} milliseconds)");

                return response;
            }

            _logger.LogInformation($"Evince Request: {requestName} ({_timer.ElapsedMilliseconds} milliseconds)");

            return response;
        }
    }
}
