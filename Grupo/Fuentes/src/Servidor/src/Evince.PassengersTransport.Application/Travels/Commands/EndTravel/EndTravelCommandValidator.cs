﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Travels.Commands.EndTravel
{
    public class EndTravelCommandValidator : AbstractValidator<EndTravelCommand>
    {
        public EndTravelCommandValidator()
        {
            RuleFor(x => x.Travel)
                .NotNull().WithMessage("El identificador del viaje no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del viaje no puede estar vacío");
        }
    }
}
