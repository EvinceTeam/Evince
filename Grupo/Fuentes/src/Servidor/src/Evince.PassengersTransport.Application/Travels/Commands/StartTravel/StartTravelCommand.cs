﻿using MediatR;

namespace Evince.PassengersTransport.Application.Travels.Commands.StartTravel
{
    public class StartTravelCommand : IRequest
    {
        public int Travel { get; set; }
    }
}
