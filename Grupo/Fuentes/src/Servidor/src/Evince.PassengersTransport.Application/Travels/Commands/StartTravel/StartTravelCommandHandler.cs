﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Travels.Commands.StartTravel
{
    public class StartTravelCommandHandler : IRequestHandler<StartTravelCommand, Unit>
    {
        private readonly ITravelRepository _travelRepository;

        public StartTravelCommandHandler(ITravelRepository travelRepository)
        {
            _travelRepository = travelRepository;
        }

        public async Task<Unit> Handle(StartTravelCommand command, CancellationToken cancellationToken)
        {
            var travel = await _travelRepository.GetByIdAsync(command.Travel, cancellationToken);

            if (travel == null)
            {
                throw new InvalidOperationException("No ha sido posible encontrar el viaje seleccionado");
            }

            travel.MarkTravelStart();

            await _travelRepository.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
