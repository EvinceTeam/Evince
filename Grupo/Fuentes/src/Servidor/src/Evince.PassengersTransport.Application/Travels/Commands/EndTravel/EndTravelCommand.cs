﻿using MediatR;

namespace Evince.PassengersTransport.Application.Travels.Commands.EndTravel
{
    public class EndTravelCommand : IRequest
    {
        public int Travel { get; set; }
    }
}
