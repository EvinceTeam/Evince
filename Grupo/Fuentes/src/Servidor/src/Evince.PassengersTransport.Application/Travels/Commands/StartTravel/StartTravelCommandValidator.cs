﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Travels.Commands.StartTravel
{
    class StartTravelCommandValidator : AbstractValidator<StartTravelCommand>
    {
        public StartTravelCommandValidator()
        {
            RuleFor(x => x.Travel)
                .NotNull().WithMessage("El identificador del viaje no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del viaje no puede estar vacío");
        }
    }
}
