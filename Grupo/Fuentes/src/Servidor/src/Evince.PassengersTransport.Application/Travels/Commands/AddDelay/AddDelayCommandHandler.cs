﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Travels.Commands.AddDelay
{
    public class AddDelayCommandHandler : IRequestHandler<AddDelayCommand, Unit>
    {
        private readonly ITravelRepository _travelRepository;

        public AddDelayCommandHandler(ITravelRepository travelRepository)
        {
            _travelRepository = travelRepository;
        }

        public async Task<Unit> Handle(AddDelayCommand command, CancellationToken cancellationToken)
        {
            var travel = await _travelRepository.GetSingleBySpecAsync(new TravelsWithDelaysSpecification(command.Travel), cancellationToken);

            if (travel == null)
            {
                throw new InvalidOperationException("No ha sido posible encontrar el viaje seleccionado");
            }

            var time = DateTime.ParseExact(command.Time, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;

            var delay = new Delay(time, command.Description, command.Type);

            travel.AddDelay(delay);

            await _travelRepository.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
