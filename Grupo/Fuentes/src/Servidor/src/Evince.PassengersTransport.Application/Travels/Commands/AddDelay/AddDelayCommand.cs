﻿using Evince.PassengersTransport.Domain.Entities;
using MediatR;

namespace Evince.PassengersTransport.Application.Travels.Commands.AddDelay
{
    public class AddDelayCommand : IRequest
    {
        public int Travel { get; set; }
        public string Description { get; set; }
        public DelayType Type { get; set; } = DelayType.Undefined;
        public string Time { get; set; }
    }
}
