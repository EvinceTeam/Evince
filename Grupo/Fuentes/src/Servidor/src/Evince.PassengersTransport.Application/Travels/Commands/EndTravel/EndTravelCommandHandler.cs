﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Travels.Commands.EndTravel
{
    public class EndTravelCommandHandler : IRequestHandler<EndTravelCommand, Unit>
    {
        private readonly ITravelRepository _travelRepository;

        public EndTravelCommandHandler(ITravelRepository travelRepository)
        {
            _travelRepository = travelRepository;
        }

        public async Task<Unit> Handle(EndTravelCommand command, CancellationToken cancellationToken)
        {
            var travel = await _travelRepository.GetByIdAsync(command.Travel, cancellationToken);

            if (travel == null)
            {
                throw new InvalidOperationException("No ha sido posible encontrar el viaje seleccionado");
            }

            travel.MarkTravelEnd();

            await _travelRepository.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
