﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Travels.Commands.AddDelay
{
    public class AddDelayCommandValidator : AbstractValidator<AddDelayCommand>
    {
        public AddDelayCommandValidator()
        {
            RuleFor(x => x.Travel)
                .NotNull().WithMessage("El identificador del viaje no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del viaje no puede estar vacío");
            RuleFor(x => x.Description)
                .NotNull().WithMessage("La descripcion del retraso no puede tener un valor nulo");
            RuleFor(x => x.Type)
                .NotNull().WithMessage("El tipo de retraso no puede tener un valor nulo");
            RuleFor(x => x.Time)
                .NotNull().WithMessage("El tiempo del retraso no puede tener un valor nulo")
                .NotEmpty().WithMessage("El tiempo del retraso no puede estar vacío")
                .Matches("^([0-1][0-9]|[2][0-3]):([0-5][0-9])$").WithMessage("El tiempo del retraso debe tener formato hh:mm");
        }
    }
}
