﻿using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelayTypes
{
    public class GetTravelDelayTypesQueryHandler : IRequestHandler<GetTravelDelayTypesQuery, IEnumerable<string>>
    {
        public Task<IEnumerable<string>> Handle(GetTravelDelayTypesQuery request, CancellationToken cancellationToken)
        {
            var types = new List<string>();

            foreach (DelayType type in Enum.GetValues(typeof(DelayType)))
            {
                types.Add(type.ToFriendlyString());
            }

            return Task.FromResult(types.AsEnumerable());
        }
    }
}
