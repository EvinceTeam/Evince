﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravel
{
    public class GetTravelQueryHandler : IRequestHandler<GetTravelQuery, TravelDto>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;

        public GetTravelQueryHandler(ITravelReadOnlyRepository travelRepository)
        {
            _travelRepository = travelRepository;
        }

        public async Task<TravelDto> Handle(GetTravelQuery request, CancellationToken cancellationToken)
        {
            var driverTravel = await GetTravelAsync(request.TravelId, cancellationToken);

            if (driverTravel == null)
            {
                throw new InvalidOperationException("No ha sido posible encontrar el viaje solicitado");
            }

            return TravelDto.MapFrom(driverTravel);
        }

        private Task<Travel> GetTravelAsync(int travelId, CancellationToken cancellationToken)
        {

            return _travelRepository.GetSingleBySpecAsync(new TravelsWithDelaysRoadMapJourneysSpecification(travelId), cancellationToken);
        }
    }
}
