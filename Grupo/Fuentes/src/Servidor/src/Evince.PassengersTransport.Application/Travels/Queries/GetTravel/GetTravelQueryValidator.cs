﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravel
{
    public class GetTravelQueryValidator : AbstractValidator<GetTravelQuery>
    {
        public GetTravelQueryValidator()
        {
            RuleFor(x => x.TravelId)
                .NotNull().WithMessage("El identificador del viaje no puede tener un valor nulo");
        }
    }
}
