﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelays
{
    public class GetTravelDelaysQueryValidator : AbstractValidator<GetTravelDelaysQuery>
    {
        public GetTravelDelaysQueryValidator()
        {
            RuleFor(x => x.TravelId)
                .NotNull().WithMessage("El identificador del viaje no puede tener un valor nulo");
        }
    }
}
