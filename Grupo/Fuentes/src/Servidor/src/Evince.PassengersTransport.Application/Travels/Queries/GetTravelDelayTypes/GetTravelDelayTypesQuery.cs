﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelayTypes
{
    public class GetTravelDelayTypesQuery : IRequest<IEnumerable<string>>
    {
    }
}
