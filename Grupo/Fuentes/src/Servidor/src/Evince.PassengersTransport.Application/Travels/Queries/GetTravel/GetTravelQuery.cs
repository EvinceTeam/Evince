﻿using MediatR;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravel
{
    public class GetTravelQuery : IRequest<TravelDto>
    {
        public int TravelId { get; set; }
    }
}
