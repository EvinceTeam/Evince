﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelays
{
    public class GetTravelDelaysQueryHandler : IRequestHandler<GetTravelDelaysQuery, IEnumerable<DelayDto>>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;

        public GetTravelDelaysQueryHandler(ITravelReadOnlyRepository travelRepository)
        {
            _travelRepository = travelRepository;
        }

        public async Task<IEnumerable<DelayDto>> Handle(GetTravelDelaysQuery request, CancellationToken cancellationToken)
        {
            var driverTravel = await GetTravelAsync(request.TravelId, cancellationToken);

            if (driverTravel == null)
            {
                throw new InvalidOperationException("No ha sido posible encontrar el viaje solicitado");
            }

            return driverTravel.Delays.Select(delay => DelayDto.MapFrom(delay));
        }

        private Task<Travel> GetTravelAsync(int travelId, CancellationToken cancellationToken)
        {
            return _travelRepository.GetSingleBySpecAsync(new TravelsWithDelaysRoadMapJourneysSpecification(travelId), cancellationToken);
        }
    }
}
