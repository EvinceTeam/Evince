﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelays
{
    public class GetTravelDelaysQuery : IRequest<IEnumerable<DelayDto>>
    {
        public int TravelId { get; set; }
    }
}
