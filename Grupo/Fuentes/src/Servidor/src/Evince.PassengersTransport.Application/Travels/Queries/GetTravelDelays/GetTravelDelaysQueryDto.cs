﻿using Evince.PassengersTransport.Domain.Entities;
using System;

namespace Evince.PassengersTransport.Application.Travels.Queries.GetTravelDelays
{
    public class DelayDto
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public TimeSpan Tiempo { get; set; }

        public static DelayDto MapFrom(Delay source)
        {
            return new DelayDto
            {
                Id = source.Id,
                Tipo = source.Type.ToFriendlyString(),
                Descripcion = source.Description,
                Tiempo = source.Time
            };
        }
    }
}
