﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMap
{
    public class GetDriverRoadMapQueryValidator : AbstractValidator<GetDriverRoadMapQuery>
    {
        public GetDriverRoadMapQueryValidator()
        {
            RuleFor(x => x.DriverId)
                .NotNull().WithMessage("El identificador del chofer no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del chofer no puede estar vacío");
        }
    }
}
