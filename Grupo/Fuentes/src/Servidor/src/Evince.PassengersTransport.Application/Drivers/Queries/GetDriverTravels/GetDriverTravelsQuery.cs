﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverTravels
{
    public class GetDriverTravelsQuery : IRequest<IEnumerable<TravelDto>>
    {
        public string DriverId { get; set; }
    }
}
