﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDrivers
{
    public class GetDriversQuery : IRequest<IEnumerable<DriverDto>>
    {
        public string Query { get; set; }
    }
}
