﻿using Evince.PassengersTransport.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverTravels
{
    public class GetDriverTravelsQueryDto
    {
        public static IEnumerable<TravelDto> MapFrom(IEnumerable<Travel> travels)
        {
            return travels.Select(t => TravelDto.MapFrom(t));
        }
    }

    public class TravelDto
    {
        public int Id { get; set; }
        public int CantPasajeros { get; set; }
        public DateTime FechaHoraSalidaEstipuladas { get; set; }
        public DateTime FechaHoraLlegadaEstipuladas { get; set; }
        public DateTime? FechaHoraRealSalida { get; set; }
        public DateTime? FechaHoraRealLlegada { get; set; }
        public int RecorridoId { get; set; }
        public JourneyDto Trayecto { get; set; }
        public string Estado { get; set; }
        public int Orden { get; set; }
        public IEnumerable<DelayDto> Retrasos { get; set; }

        public static TravelDto MapFrom(Travel source)
        {
            var estado = string.Empty;

            switch (source.State)
            {
                case TravelState.Finalized:
                    estado = "detenido";
                    break;
                case TravelState.InProgress:
                    estado = "actual";
                    break;
                case TravelState.Pending:
                    estado = "pendiente";
                    break;
            }

            return new TravelDto
            {
                Id = source.Id,
                CantPasajeros = source.NumberOfPassengers,
                FechaHoraSalidaEstipuladas = source.StipulatedStartTime,
                FechaHoraLlegadaEstipuladas = source.StipulatedEndTime,
                FechaHoraRealSalida = source.StartTime,
                FechaHoraRealLlegada = source.EndTime,
                RecorridoId = source.RoadMap.Id,
                Trayecto = JourneyDto.MapFrom(source.Journey),
                Estado = estado,
                Orden = source.RoadmapTravel.Order,
                Retrasos = source.Delays.Select(d => DelayDto.MapFrom(d))
            };
        }
    }

    public class JourneyDto
    {
        public int Id { get; set; }
        public string TerminalOrigen { get; set; }
        public int TerminalOrigenId { get; set; }
        public string TerminalDestino { get; set; }
        public int TerminalDestinoId { get; set; }

        public static JourneyDto MapFrom(Journey source)
        {
            return new JourneyDto
            {
                Id = source.Id,
                TerminalOrigen = source.StartNode.Code,
                TerminalOrigenId = source.StartNode.Id,
                TerminalDestino = source.EndNode.Code,
                TerminalDestinoId = source.EndNode.Id
            };
        }
    }

    public class DelayDto
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public TimeSpan Tiempo { get; set; }

        public static DelayDto MapFrom(Delay source)
        {
            return new DelayDto
            {
                Id = source.Id,
                Tipo = source.Type.ToFriendlyString(),
                Descripcion = source.Description,
                Tiempo = source.Time
            };
        }
    }
}
