﻿using MediatR;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMap
{
    public class GetDriverRoadMapQuery : IRequest<RoadMapDto>
    {
        public string DriverId { get; set; }
    }
}
