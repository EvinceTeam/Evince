﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Application.Drivers.Notifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverTravels
{
    public class GetDriverTravelsQueryHandler : IRequestHandler<GetDriverTravelsQuery, IEnumerable<TravelDto>>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;
        private readonly IMediator _mediator;

        public GetDriverTravelsQueryHandler(ITravelReadOnlyRepository travelRepository, IMediator mediator)
        {
            _travelRepository = travelRepository;
            _mediator = mediator;
        }

        public async Task<IEnumerable<TravelDto>> Handle(GetDriverTravelsQuery request, CancellationToken cancellationToken)
        {
            var driverTravels = await GetDriverActiveTravelsAsync(request.DriverId, cancellationToken);

            if (!driverTravels.Any())
            {
                await _mediator.Publish(new DriverWithoutTravelNotification { DriverId = request.DriverId }, cancellationToken);

                driverTravels = await GetDriverActiveTravelsAsync(request.DriverId, cancellationToken);
            }

            return GetDriverTravelsQueryDto.MapFrom(driverTravels);
        }

        private async Task<IEnumerable<Travel>> GetDriverActiveTravelsAsync(string driverId, CancellationToken cancellationToken)
        {
            var travels = await _travelRepository.ListAsync(new TravelsWithDelaysRoadMapJourneysSpecification(), cancellationToken);

            return travels.Where(travel =>
                travel.State != TravelState.Finalized &&
                travel.DriverId == driverId
            );
        }
    }
}
