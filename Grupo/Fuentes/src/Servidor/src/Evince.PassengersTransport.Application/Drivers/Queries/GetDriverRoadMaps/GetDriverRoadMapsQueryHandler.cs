﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Application.Drivers.Notifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMaps
{
    public class GetDriverRoadMapsQueryHandler : IRequestHandler<GetDriverRoadMapsQuery, IEnumerable<RoadMapDto>>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;
        private readonly IMediator _mediator;

        public GetDriverRoadMapsQueryHandler(ITravelReadOnlyRepository travelRepository, IMediator mediator)
        {
            _travelRepository = travelRepository;
            _mediator = mediator;
        }

        public async Task<IEnumerable<RoadMapDto>> Handle(GetDriverRoadMapsQuery request, CancellationToken cancellationToken)
        {
            var driverTravels = await GetDriverActiveTravelsAsync(request.DriverId, cancellationToken);

            if (!driverTravels.Any())
            {
                await _mediator.Publish(new DriverWithoutTravelNotification { DriverId = request.DriverId }, cancellationToken);

                driverTravels = await GetDriverActiveTravelsAsync(request.DriverId, cancellationToken);
            }

            var driverRoadmaps = driverTravels.GroupBy(t => t.RoadMap)
                .ToDictionary(g =>
                    g.Key,
                    g => g.Select(t => t).OrderBy(t => t.RoadmapTravel.Order).ThenByDescending(t => t.StipulatedStartTime).AsEnumerable()
                );

            return GetDriverRoadMapsQueryDto.MapFrom(driverRoadmaps);
        }

        private async Task<IEnumerable<Travel>> GetDriverActiveTravelsAsync(string driverId, CancellationToken cancellationToken)
        {
            var travels = await _travelRepository.ListAsync(new TravelsWithDelaysRoadMapJourneysSpecification(), cancellationToken);

            return travels.Where(travel =>
                travel.DriverId == driverId
            );
        }
    }
}
