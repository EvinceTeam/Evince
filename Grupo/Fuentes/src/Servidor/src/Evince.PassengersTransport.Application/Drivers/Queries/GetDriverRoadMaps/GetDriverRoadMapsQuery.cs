﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMaps
{
    public class GetDriverRoadMapsQuery : IRequest<IEnumerable<RoadMapDto>>
    {
        public string DriverId { get; set; }
    }
}
