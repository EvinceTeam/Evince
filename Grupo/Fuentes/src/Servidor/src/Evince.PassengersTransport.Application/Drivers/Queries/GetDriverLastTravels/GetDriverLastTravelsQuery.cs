﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverLastTravels
{
    public class GetDriverLastTravelsQuery : IRequest<IEnumerable<TravelDto>>
    {
        public string DriverId { get; set; }
        public int? Amount { get; set; }
    }
}
