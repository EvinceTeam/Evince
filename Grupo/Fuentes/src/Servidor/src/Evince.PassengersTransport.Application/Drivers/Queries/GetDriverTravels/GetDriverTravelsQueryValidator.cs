﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverTravels
{
    public class GetDriverTravelsQueryValidator : AbstractValidator<GetDriverTravelsQuery>
    {
        public GetDriverTravelsQueryValidator()
        {
            RuleFor(x => x.DriverId)
                .NotNull().WithMessage("El identificador del chofer no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del chofer no puede estar vacío");
        }
    }
}
