﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Application.Drivers.Notifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMap
{
    public class GetDriverRoadMapQueryHandler : IRequestHandler<GetDriverRoadMapQuery, RoadMapDto>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;
        private readonly IRouteReadOnlyRepository _routeRepository;
        private readonly IMediator _mediator;

        public GetDriverRoadMapQueryHandler(
            ITravelReadOnlyRepository travelRepository,
            IRouteReadOnlyRepository routeRepository,
            IMediator mediator)
        {
            _travelRepository = travelRepository;
            _routeRepository = routeRepository;
            _mediator = mediator;
        }

        public async Task<RoadMapDto> Handle(GetDriverRoadMapQuery request, CancellationToken cancellationToken)
        {
            var driverTravels = await GetDriverTravelsAsync(request.DriverId, cancellationToken);

            if (!driverTravels.Any())
            {
                await _mediator.Publish(new DriverWithoutTravelNotification { DriverId = request.DriverId }, cancellationToken);

                driverTravels = await GetDriverTravelsAsync(request.DriverId, cancellationToken);
            }

            return GetDriverRoadMapQueryDto.MapFrom(driverTravels);
        }

        private async Task<IEnumerable<Travel>> GetDriverTravelsAsync(string driverId, CancellationToken cancellationToken)
        {
            var travels = await _travelRepository.ListAsync(new TravelsWithDelaysRoadMapJourneysSpecification(), cancellationToken);

            var activeDriverTravel = travels
                .Where(travel =>
                    travel.DriverId == driverId &&
                    travel.State != TravelState.Finalized)
                .OrderBy(t => t.StipulatedStartTime)
                .FirstOrDefault();

            if (activeDriverTravel == null)
            {
                return Enumerable.Empty<Travel>();
            }

            var route = await _routeRepository.GetSingleBySpecAsync(new RouteWithRoadMapTravelsSpecification(activeDriverTravel.RouteId), cancellationToken);

            return route.Travels;
        }
    }
}
