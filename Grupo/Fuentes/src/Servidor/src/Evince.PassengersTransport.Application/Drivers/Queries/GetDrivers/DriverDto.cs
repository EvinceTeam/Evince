﻿using Evince.PassengersTransport.Domain.Entities;
using System;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDrivers
{
    public class DriverDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }

        public static DriverDto MapFrom(Driver source)
        {
            return new DriverDto
            {
                Id = source.Id,
                Name = source.Name,
                Lastname = source.Lastname
            };
        }
    }
}