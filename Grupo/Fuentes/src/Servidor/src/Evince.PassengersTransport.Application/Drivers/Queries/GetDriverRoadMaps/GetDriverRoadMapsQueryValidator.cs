﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMaps
{
    public class GetDriverRoadMapsQueryValidator : AbstractValidator<GetDriverRoadMapsQuery>
    {
        public GetDriverRoadMapsQueryValidator()
        {
            RuleFor(x => x.DriverId)
                .NotNull().WithMessage("El identificador del chofer no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del chofer no puede estar vacío");
        }
    }
}
