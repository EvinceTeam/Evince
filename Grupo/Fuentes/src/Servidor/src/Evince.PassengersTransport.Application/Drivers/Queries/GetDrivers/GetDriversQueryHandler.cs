﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDrivers
{
    public class GetDriversQueryHandler : IRequestHandler<GetDriversQuery, IEnumerable<DriverDto>>
    {
        private readonly IDriverReadOnlyRepository _driverRepository;

        public GetDriversQueryHandler(IDriverReadOnlyRepository driverRepository)
        {
            _driverRepository = driverRepository;
        }

        public async Task<IEnumerable<DriverDto>> Handle(GetDriversQuery request, CancellationToken cancellationToken)
        {
            var drivers = await GetDrivers(cancellationToken);

            if (!string.IsNullOrEmpty(request.Query))
            {
                drivers = drivers.Where(d => d.Fullname.ToLower().Contains(request.Query.ToLower()));
            }

            return drivers.Select(d => DriverDto.MapFrom(d));
        }

        private Task<IEnumerable<Driver>> GetDrivers(CancellationToken cancellationToken)
        {
            return _driverRepository.ListAllAsync(cancellationToken);
        }
    }
}
