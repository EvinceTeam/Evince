﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverLastTravels
{
    public class GetDriverLastTravelsQueryHandler : IRequestHandler<GetDriverLastTravelsQuery, IEnumerable<TravelDto>>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;

        public GetDriverLastTravelsQueryHandler(ITravelReadOnlyRepository travelRepository)
        {
            _travelRepository = travelRepository;
        }

        public async Task<IEnumerable<TravelDto>> Handle(GetDriverLastTravelsQuery request, CancellationToken cancellationToken)
        {
            var amount = request.Amount ?? 10;

            var driverTravels = await GetDriverFinalizedTravelsAsync(request.DriverId, cancellationToken);

            if (driverTravels.Count() >= amount)
            {
                driverTravels = driverTravels.TakeLast(amount);
            }

            return GetDriverLastTravelsQueryDto.MapFrom(driverTravels);
        }

        private async Task<IEnumerable<Travel>> GetDriverFinalizedTravelsAsync(string driverId, CancellationToken cancellationToken)
        {
            var travels = await _travelRepository.ListAsync(new TravelsWithDelaysRoadMapJourneysSpecification(), cancellationToken);

            return travels.Where(travel =>
                travel.State == TravelState.Finalized &&
                travel.DriverId == driverId
            );
        }
    }
}
