﻿using Evince.PassengersTransport.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.PassengersTransport.Application.Drivers.Queries.GetDriverRoadMaps
{
    public class GetDriverRoadMapsQueryDto
    {
        public static IEnumerable<RoadMapDto> MapFrom(IDictionary<RoadMap, IEnumerable<Travel>> source)
        {
            var recorridos = new List<RoadMapDto>();

            var order = 0;

            foreach (var roadmap in source.Keys)
            {
                recorridos.Add(RoadMapDto.MapFrom(roadmap, source[roadmap], order));

                order++;
            }

            return recorridos;
        }
    }

    public class RoadMapDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public int Orden { get; set; }
        public IEnumerable<TravelDto> Viajes { get; set; }
        public IEnumerable<JourneyDto> Trayectos { get; set; }

        public static RoadMapDto MapFrom(RoadMap roadmap, IEnumerable<Travel> travels, int order)
        {
            return new RoadMapDto
            {
                Id = roadmap.Id,
                Nombre = $"{roadmap.FromCode} - {roadmap.ToCode}",
                Descripcion = $"{roadmap.From} - {roadmap.To}",
                Orden = order,
                Viajes = travels.Select(item => TravelDto.MapFrom(item)),
                Trayectos = roadmap.Items.OrderBy(i => i.Order).Select(i => JourneyDto.MapFrom(i.Journey))
            };
        }
    }

    public class TravelDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public int CantPasajeros { get; set; }
        public DateTime FechaHoraSalidaEstipuladas { get; set; }
        public DateTime FechaHoraLlegadaEstipuladas { get; set; }
        public DateTime? FechaHoraRealSalida { get; set; }
        public DateTime? FechaHoraRealLlegada { get; set; }
        public int RecorridoId { get; set; }
        public JourneyDto Trayecto { get; set; }
        public int Estado { get; set; }
        public int Orden { get; set; }
        public IEnumerable<DelayDto> Retrasos { get; set; }

        public static TravelDto MapFrom(Travel source)
        {
            return new TravelDto
            {
                Id = source.Id,
                Nombre = source.Name,
                CantPasajeros = source.NumberOfPassengers,
                FechaHoraSalidaEstipuladas = source.StipulatedStartTime,
                FechaHoraLlegadaEstipuladas = source.StipulatedEndTime,
                FechaHoraRealSalida = source.StartTime,
                FechaHoraRealLlegada = source.EndTime,
                RecorridoId = source.RoadMap.Id,
                Trayecto = JourneyDto.MapFrom(source.Journey),
                Estado = (int)source.State,
                Orden = source.RoadmapTravel.Order,
                Retrasos = source.Delays.Select(d => DelayDto.MapFrom(d))
            };
        }
    }

    public class JourneyDto
    {
        public int Id { get; set; }
        public string TerminalOrigen { get; set; }
        public string TerminalOrigenCodigo { get; set; }
        public int TerminalOrigenId { get; set; }
        public string TerminalDestino { get; set; }
        public string TerminalDestinoCodigo { get; set; }
        public int TerminalDestinoId { get; set; }

        public static JourneyDto MapFrom(Journey source)
        {
            return new JourneyDto
            {
                Id = source.Id,
                TerminalOrigen = source.StartNode.ShortDescription,
                TerminalOrigenCodigo = source.StartNode.Code,
                TerminalOrigenId = source.StartNode.Id,
                TerminalDestino = source.EndNode.ShortDescription,
                TerminalDestinoCodigo = source.EndNode.Code,
                TerminalDestinoId = source.EndNode.Id
            };
        }
    }

    public class DelayDto
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public TimeSpan Tiempo { get; set; }

        public static DelayDto MapFrom(Delay source)
        {
            return new DelayDto
            {
                Id = source.Id,
                Tipo = source.Type.ToFriendlyString(),
                Descripcion = source.Description,
                Tiempo = source.Time
            };
        }
    }
}
