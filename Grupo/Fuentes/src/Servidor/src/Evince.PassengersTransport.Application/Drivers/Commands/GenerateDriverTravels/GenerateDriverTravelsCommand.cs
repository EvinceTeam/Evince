﻿using MediatR;

namespace Evince.PassengersTransport.Application.Drivers.Commands.GenerateDriverTravels
{
    public class GenerateDriverTravelsCommand : IRequest
    {
        public string DriverId { get; set; }
    }
}
