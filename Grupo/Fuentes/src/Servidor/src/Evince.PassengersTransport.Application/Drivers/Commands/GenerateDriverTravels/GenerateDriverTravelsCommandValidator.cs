﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Drivers.Commands.GenerateDriverTravels
{
    public class GenerateDriverTravelsCommandValidator : AbstractValidator<GenerateDriverTravelsCommand>
    {
        public GenerateDriverTravelsCommandValidator()
        {
            RuleFor(x => x.DriverId)
                .NotNull().WithMessage("El identificador del chofer no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del chofer no puede estar vacío");
        }
    }
}
