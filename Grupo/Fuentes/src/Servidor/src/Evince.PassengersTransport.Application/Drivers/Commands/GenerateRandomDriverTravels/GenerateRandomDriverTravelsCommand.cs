﻿using MediatR;

namespace Evince.PassengersTransport.Application.Drivers.Commands.GenerateRandomDriverTravels
{
    public class GenerateRandomDriverTravelsCommand : IRequest
    {
        public string DriverId { get; set; }
    }
}
