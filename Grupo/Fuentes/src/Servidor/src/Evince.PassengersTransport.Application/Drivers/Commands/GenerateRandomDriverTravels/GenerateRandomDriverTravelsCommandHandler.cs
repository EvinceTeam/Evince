﻿using Evince.Application.Interfaces.Persistance;
using Evince.PassengersTransport.Application.Drivers.Notifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Drivers.Commands.GenerateRandomDriverTravels
{
    public class GenerateRandomDriverTravelsCommandHandler : IRequestHandler<GenerateRandomDriverTravelsCommand, Unit>, INotificationHandler<DriverWithoutTravelNotification>
    {
        private readonly IEvinceDbContext _context;

        public GenerateRandomDriverTravelsCommandHandler(IEvinceDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(GenerateRandomDriverTravelsCommand command, CancellationToken cancellationToken)
        {
            var driverActiveRoadMapsCount = await GetNumberOfActiveDriverRoadMapsAsync(command.DriverId, cancellationToken);

            while (driverActiveRoadMapsCount < 5)
            {
                var driverTravels = await GenerateTravelsForDriverAsync(command.DriverId, cancellationToken);

                _context.Travels.AddRange(driverTravels);

                driverActiveRoadMapsCount++;

                await _context.SaveChangesAsync(cancellationToken);
            }

            return Unit.Value;
        }

        public Task Handle(DriverWithoutTravelNotification notification, CancellationToken cancellationToken)
        {
            return Handle(new GenerateRandomDriverTravelsCommand { DriverId = notification.DriverId }, cancellationToken);
        }

        private async Task<IEnumerable<Travel>> GenerateTravelsForDriverAsync(string driverId, CancellationToken cancellationToken)
        {
            var roadMap = await GetRandomRoadMapAsync(cancellationToken);

            var route = new Route(roadMap);

            var stipulatedStartTime = DateTime.Now.AddHours(1);

            var lastTravel = await GetLastDriverTravelAsync(driverId, cancellationToken);

            if (lastTravel != null)
            {
                var possibleStartTime = lastTravel.StipulatedEndTime.AddHours(3);
                stipulatedStartTime = possibleStartTime <= DateTime.Now ? DateTime.Now.AddHours(3) : possibleStartTime;
            }

            var travels = new List<Travel>();

            var orderedRoadmapItems = roadMap.Items.OrderBy(i => i.Order);

            foreach (var roadMapItem in orderedRoadmapItems)
            {
                var travel = await GenerateTravelAsync(driverId, stipulatedStartTime, roadMap, roadMapItem.Journey, route, cancellationToken);

                travels.Add(travel);

                stipulatedStartTime = travel.StipulatedEndTime.AddMinutes(15);
            }

            return travels;
        }

        private async Task<Travel> GetLastDriverTravelAsync(string driverId, CancellationToken cancellationToken)
        {
            var travels = await _context.Travels
                .Include(t => t.Journey)
                .Where(t => t.DriverId == driverId)
                .ToListAsync(cancellationToken);

            travels = travels
                .OrderBy(t => t.StipulatedEndTime)
                .ToList();

            return travels.LastOrDefault();
        }

        private async Task<int> GetNumberOfActiveDriverRoadMapsAsync(string driverId, CancellationToken cancellationToken)
        {
            var driverActiveRoadmaps = await _context.Travels
                .Where(t => t.DriverId == driverId && t.State != TravelState.Finalized)
                .GroupBy(t => t.RoadMap)
                .ToDictionaryAsync(g => g.Key, g => g.Select(t => t), cancellationToken);

            return driverActiveRoadmaps.Keys.Count;
        }

        private async Task<RoadMap> GetRandomRoadMapAsync(CancellationToken cancellationToken)
        {
            var roadMaps = await _context
                .RoadMaps
                .Include(r => r.Items)
                .ThenInclude(ri => ri.Journey)
                .ToListAsync(cancellationToken);

            var random = new Random();
            var roadMapIndex = random.Next(roadMaps.Count);

            return roadMaps[roadMapIndex];
        }

        private async Task<Travel> GenerateTravelAsync(string driverId, DateTime stipulatedStartTime, RoadMap roadMap, Journey journey, Route route, CancellationToken cancellationToken)
        {
            var bus = await GetBusAsync(cancellationToken);

            if (bus == null)
            {
                throw new InvalidOperationException("No hay colectivos disponibles en este momento");
            }

            var numberOfPassengers = GetRandomPassengerAmount(bus);
            var driver = GetDriver(driverId);

            var travel = new Travel(
                stipulatedStartTime,
                null,
                null,
                numberOfPassengers,
                roadMap,
                journey,
                driver,
                bus,
                route
            );

            return travel;
        }

        private Driver GetDriver(string driverId)
        {
            return _context
                .Drivers
                .Single(d => d.Id == driverId);
        }

        private async Task<Bus> GetBusAsync(CancellationToken cancellationToken)
        {
            var activeBuses = await _context
                .Travels
                .Where(travel => travel.State != TravelState.Finalized)
                .Select(travel => travel.BusId)
                .ToListAsync(cancellationToken);

            return await _context
                .Buses
                .Where(bus => !activeBuses.Contains(bus.Id))
                .FirstOrDefaultAsync(cancellationToken);
        }

        private int GetRandomPassengerAmount(Bus bus)
        {
            var random = new Random();

            return random.Next(1, bus.Capacity);
        }
    }
}
