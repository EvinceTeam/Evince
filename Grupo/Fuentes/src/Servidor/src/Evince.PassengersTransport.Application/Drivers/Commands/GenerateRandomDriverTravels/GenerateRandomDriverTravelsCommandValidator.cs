﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Drivers.Commands.GenerateRandomDriverTravels
{
    public class GenerateRandomDriverTravelsCommandValidator : AbstractValidator<GenerateRandomDriverTravelsCommand>
    {
        public GenerateRandomDriverTravelsCommandValidator()
        {
            RuleFor(x => x.DriverId)
                .NotNull().WithMessage("El identificador del chofer no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del chofer no puede estar vacío");
        }
    }
}
