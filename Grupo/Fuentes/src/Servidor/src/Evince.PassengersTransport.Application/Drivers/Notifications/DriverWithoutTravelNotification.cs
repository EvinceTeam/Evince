﻿using MediatR;

namespace Evince.PassengersTransport.Application.Drivers.Notifications
{
    public class DriverWithoutTravelNotification : INotification
    {
        public string DriverId { get; set; }
    }
}
