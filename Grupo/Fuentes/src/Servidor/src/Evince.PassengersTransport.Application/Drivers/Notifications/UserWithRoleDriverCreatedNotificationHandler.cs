﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.Common.Events;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Drivers.Notifications
{
    public class UserWithRoleDriverCreatedNotificationHandler : INotificationHandler<UserWithRoleDriverCreatedNotification>
    {
        private readonly IDriverRepository _driverRepository;

        public UserWithRoleDriverCreatedNotificationHandler(IDriverRepository driverRepository)
        {
            _driverRepository = driverRepository;
        }

        public async Task Handle(UserWithRoleDriverCreatedNotification notification, CancellationToken cancellationToken)
        {
            var driver = new Driver(notification.Id, notification.Name, notification.Lastname);

            await _driverRepository.AddAsync(driver, cancellationToken);
        }
    }
}
