﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.RoadMaps.Commands.CreateRoadMap
{
    class CreateRoadMapCommandHandler : IRequestHandler<CreateRoadMapCommand, Unit>
    {
        private readonly IRoadMapRepository _roadMapRepository;
        private readonly IJourneyRepository _journeyRepository;

        public CreateRoadMapCommandHandler(IRoadMapRepository roadMapRepository, IJourneyRepository journeyRepository)
        {
            _roadMapRepository = roadMapRepository;
            _journeyRepository = journeyRepository;
        }

        public async Task<Unit> Handle(CreateRoadMapCommand command, CancellationToken cancellationToken)
        {
            if (command.JourneysIds.Any(id => id == 0))
            {
                throw new InvalidOperationException("Existen trayectos con un identificador inválido");
            }

            var isUnique = await RoadMapIsUnique(command.JourneysIds, cancellationToken);

            if (!isUnique)
            {
                throw new InvalidOperationException("Ya existe un recorrido con los mismos trayectos");
            }

            var roadMap = new RoadMap();

            foreach (var journeyId in command.JourneysIds)
            {
                var journey = await _journeyRepository.GetByIdAsync(journeyId, cancellationToken);

                if (journey == null)
                {
                    throw new InvalidOperationException("No ha sido posible encontrar uno de los trayectos");
                }

                roadMap.AddRouteItem(journey);
            }

            await _roadMapRepository.AddAsync(roadMap, cancellationToken);

            return Unit.Value;
        }

        private async Task<bool> RoadMapIsUnique(IEnumerable<int> journeysIds, CancellationToken cancellationToken)
        {
            var roadMaps = await _roadMapRepository.ListAsync(new RoadMapWithJourneysSpecification(), cancellationToken);

            var roadMapExists = roadMaps.Any(roadMap =>
            {
                var roadMapJourneys = roadMap.Items.OrderBy(i => i.Order).Select(ri => ri.Journey.Id);

                return roadMapJourneys.Intersect(journeysIds).Count() == journeysIds.Count();
            });

            return !roadMapExists;
        }
    }
}
