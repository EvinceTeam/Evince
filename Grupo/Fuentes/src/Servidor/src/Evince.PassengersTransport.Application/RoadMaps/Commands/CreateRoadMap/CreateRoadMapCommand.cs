﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.RoadMaps.Commands.CreateRoadMap
{
    public class CreateRoadMapCommand : IRequest
    {
        public IEnumerable<int> JourneysIds { get; set; }
    }
}
