﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.Terminals.Queries.GetTerminalList
{
    public class GetTerminalListQuery : IRequest<IEnumerable<TerminalDto>>
    {
        public string Query { get; set; }
    }
}
