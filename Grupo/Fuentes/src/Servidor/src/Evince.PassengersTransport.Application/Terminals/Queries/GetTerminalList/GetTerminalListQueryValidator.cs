﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Terminals.Queries.GetTerminalList
{
    class GetTerminalListQueryValidator : AbstractValidator<GetTerminalListQuery>
    {
        public GetTerminalListQueryValidator()
        {
            RuleFor(x => x.Query).NotNull().WithMessage("La query no puede tener un valor nulo");
        }
    }
}
