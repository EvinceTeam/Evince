﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Terminals.Queries.GetTerminalList
{
    public class GetTerminalListQueryHandler : IRequestHandler<GetTerminalListQuery, IEnumerable<TerminalDto>>
    {
        private readonly ITerminalReadOnlyRepository _terminalRepository;
        private readonly IMemoryCache _cache;

        public GetTerminalListQueryHandler(ITerminalReadOnlyRepository terminalRepository, IMemoryCache cache)
        {
            _terminalRepository = terminalRepository;
            _cache = cache;
        }

        public async Task<IEnumerable<TerminalDto>> Handle(GetTerminalListQuery request, CancellationToken cancellationToken)
        {
            var cachedTerminals = await _cache.GetOrCreateAsync("Query-GetTerminalList", async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600);

                var terminals = await _terminalRepository.ListAllAsync(cancellationToken);

                return terminals.Select(terminal => TerminalDto.MapFrom(terminal));
            });

            var filteredTerminals = string.IsNullOrEmpty(request.Query) ?
                cachedTerminals :
                cachedTerminals.Where(t => t.Name.ToLower().Contains(request.Query.ToLower()));

            return filteredTerminals;
        }
    }
}
