﻿using Evince.PassengersTransport.Domain.Entities;

namespace Evince.PassengersTransport.Application.Terminals.Queries.GetTerminalList
{
    public class TerminalDto
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public static TerminalDto MapFrom(Terminal source)
        {
            return new TerminalDto
            {
                Id = source.Id,
                Name = source.Name
            };
        }
    }
}
