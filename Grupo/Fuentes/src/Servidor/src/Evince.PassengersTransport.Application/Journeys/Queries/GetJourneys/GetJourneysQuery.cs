﻿using MediatR;
using System.Collections.Generic;

namespace Evince.PassengersTransport.Application.Journeys.Queries.GetJourneys
{
    public class GetJourneysQuery : IRequest<IEnumerable<JourneyDto>>
    {
        public string Query { get; set; }
    }
}
