﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Journeys.Queries.GetJourneys
{
    public class GetJourneysQueryHandler : IRequestHandler<GetJourneysQuery, IEnumerable<JourneyDto>>
    {
        private readonly IJourneyReadOnlyRepository _journeyRepository;
        private readonly IMemoryCache _cache;

        public GetJourneysQueryHandler(IJourneyReadOnlyRepository journeyRepository, IMemoryCache cache)
        {
            _journeyRepository = journeyRepository;
            _cache = cache;
        }

        public async Task<IEnumerable<JourneyDto>> Handle(GetJourneysQuery request, CancellationToken cancellationToken)
        {
            var cachedJourneys = await _cache.GetOrCreateAsync("Query-GetJourneys", async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(3600);

                var journeys = await _journeyRepository.ListAsync(new JourneyWithNodesSpecification(), cancellationToken);

                return journeys.Select(journey => JourneyDto.MapFrom(journey));
            });

            var filteredJourneys = string.IsNullOrEmpty(request.Query) ?
                cachedJourneys :
                cachedJourneys.Where(j => j.Nombre.ToLower().Contains(request.Query.ToLower()));

            return filteredJourneys;
        }
    }
}
