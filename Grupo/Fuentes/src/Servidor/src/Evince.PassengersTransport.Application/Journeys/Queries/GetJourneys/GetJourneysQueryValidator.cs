﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Journeys.Queries.GetJourneys
{
    class GetJourneysQueryValidator : AbstractValidator<GetJourneysQuery>
    {
        public GetJourneysQueryValidator()
        {
            RuleFor(x => x.Query).NotNull().WithMessage("La query no puede tener un valor nulo");
        }
    }
}
