﻿using Evince.PassengersTransport.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.PassengersTransport.Application.Journeys.Queries.GetJourneys
{
    public class GetJourneysQueryDto
    {
        public static IEnumerable<JourneyDto> MapFrom(IEnumerable<Journey> source)
        {
            return source.Select(j => JourneyDto.MapFrom(j));
        }
    }

    public class JourneyDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string TerminalOrigen { get; set; }
        public string TerminalOrigenCodigo { get; set; }
        public string TerminalOrigenDescripcion { get; set; }
        public int TerminalOrigenId { get; set; }
        public string TerminalDestino { get; set; }
        public string TerminalDestinoCodigo { get; set; }
        public string TerminalDestinoDescripcion { get; set; }
        public int TerminalDestinoId { get; set; }

        public static JourneyDto MapFrom(Journey source)
        {
            return new JourneyDto
            {
                Id = source.Id,
                Nombre = source.Description,
                TerminalOrigen = source.StartNodeDescription,
                TerminalOrigenCodigo = source.StartNode.Code,
                TerminalOrigenDescripcion = source.StartNode.ShortDescription,
                TerminalOrigenId = source.StartNode.Id,
                TerminalDestino = source.EndNodeDescription,
                TerminalDestinoCodigo = source.EndNode.Code,
                TerminalDestinoDescripcion = source.EndNode.ShortDescription,
                TerminalDestinoId = source.EndNode.Id,
            };
        }
    }
}
