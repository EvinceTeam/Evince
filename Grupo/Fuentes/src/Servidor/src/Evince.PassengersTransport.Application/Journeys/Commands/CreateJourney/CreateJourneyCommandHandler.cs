﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Journeys.Commands.CreateJourney
{
    public class CreateJourneyCommandHandler : IRequestHandler<CreateJourneyCommand, Unit>
    {
        private readonly IJourneyRepository _journeyRepository;
        private readonly ITerminalRepository _terminalRepository;

        public CreateJourneyCommandHandler(IJourneyRepository journeyRepository, ITerminalRepository terminalRepository)
        {
            _journeyRepository = journeyRepository;
            _terminalRepository = terminalRepository;
        }

        public async Task<Unit> Handle(CreateJourneyCommand command, CancellationToken cancellationToken)
        {
            var startNode = await _terminalRepository.GetByIdAsync(command.StartNodeId, cancellationToken);
            var endNode = await _terminalRepository.GetByIdAsync(command.EndNodeId, cancellationToken);

            if (startNode == null || endNode == null)
            {
                throw new InvalidOperationException("Las terminales elegidas son inválidas");
            }

            var isUnique = await JourneyIsUnique(startNode, endNode, cancellationToken);

            if (!isUnique)
            {
                throw new InvalidOperationException("Ya existe un trayecto con las terminales elegidas");
            }

            var journey = new Journey(startNode, endNode, command.Duration);

            await _journeyRepository.AddAsync(journey, cancellationToken);

            return Unit.Value;
        }

        private async Task<bool> JourneyIsUnique(Terminal startNode, Terminal endNode, CancellationToken cancellationToken)
        {
            var journey = await _journeyRepository.GetSingleBySpecAsync(
                new JourneyWithNodesSpecification(startNode.Id, endNode.Id),
                cancellationToken);

            return journey == null;
        }
    }
}
