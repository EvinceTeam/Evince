﻿using MediatR;
using System;

namespace Evince.PassengersTransport.Application.Journeys.Commands.CreateJourney
{
    public class CreateJourneyCommand : IRequest
    {
        public int StartNodeId { get; set; }
        public int EndNodeId { get; set; }
        public TimeSpan Duration { get; set; }
    }
}
