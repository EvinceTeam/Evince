﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Journeys.Commands.CreateJourney
{
    public class CreateJourneyCommandValidator : AbstractValidator<CreateJourneyCommand>
    {
        public CreateJourneyCommandValidator()
        {
            RuleFor(x => x.StartNodeId)
                .GreaterThan(0).WithMessage("El identificador del la terminal de partida no puede ser 0");
            RuleFor(x => x.EndNodeId)
                .GreaterThan(0).WithMessage("El identificador del la terminal de llegada no puede ser 0");
            RuleFor(x => x.Duration)
                .NotNull().WithMessage("El tiempo del trayecto no puede ser nulo");
        }
    }
}
