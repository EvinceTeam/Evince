﻿using FluentValidation;

namespace Evince.PassengersTransport.Application.Assignments.CreateAssignment
{
    public class CreateAssignmentCommandValidator : AbstractValidator<CreateAssignmentCommand>
    {
        public CreateAssignmentCommandValidator()
        {
            RuleFor(x => x.DriverId)
                .NotNull().WithMessage("El identificador del chofer no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del chofer no puede estar vacío");
            RuleFor(x => x.RoadMapId)
                .NotNull().WithMessage("El identificador del recorrido no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del recorrido no puede estar vacío");
            RuleFor(x => x.DayOfWeek)
                .NotNull().WithMessage("El dia no puede tener un valor nulo");
            RuleFor(x => x.StartTime)
                .NotNull().WithMessage("El horario de inicio no puede tener un valor nulo");
        }
    }
}
