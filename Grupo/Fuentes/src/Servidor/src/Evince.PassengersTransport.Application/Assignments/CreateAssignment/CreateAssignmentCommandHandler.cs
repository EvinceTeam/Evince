﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.PassengersTransport.Application.Assignments.CreateAssignment
{
    public class CreateAssignmentCommandHandler : IRequestHandler<CreateAssignmentCommand, Unit>
    {
        private readonly IDriverAssignmentRepository _driverAssignmentRepository;
        private readonly IDriverRepository _driverRepository;
        private readonly IRoadMapRepository _roadMapRepository;

        public CreateAssignmentCommandHandler(
            IDriverAssignmentRepository driverAssignmentRepository,
            IDriverRepository driverRepository,
            IRoadMapRepository roadMapRepository
            )
        {
            _driverAssignmentRepository = driverAssignmentRepository;
            _driverRepository = driverRepository;
            _roadMapRepository = roadMapRepository;
        }

        public async Task<Unit> Handle(CreateAssignmentCommand command, CancellationToken cancellationToken)
        {
            var driverAssignment = await GetDriverAssignment(command.DriverId, cancellationToken);

            if (driverAssignment == null)
            {
                var driver = await GetDriver(command.DriverId, cancellationToken);

                driverAssignment = new DriverAssignment(driver);

                await _driverAssignmentRepository.AddAsync(driverAssignment, cancellationToken);
            }

            var roadmap = await GetRoadMap(command.RoadMapId, cancellationToken);

            var assignment = new Assignment(roadmap, command.DayOfWeek, command.StartTime);

            driverAssignment.AddAssignment(assignment);

            await _driverAssignmentRepository.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }

        private Task<DriverAssignment> GetDriverAssignment(string driverId, CancellationToken cancellationToken)
        {
            return _driverAssignmentRepository.GetSingleBySpecAsync(new DriverAssignmentWithAssignmentsSpecification(driverId), cancellationToken);
        }

        private Task<Driver> GetDriver(string driverId, CancellationToken cancellationToken)
        {
            return _driverRepository.GetByIdAsync(driverId, cancellationToken);
        }

        private Task<RoadMap> GetRoadMap(int roadmapId, CancellationToken cancellationToken)
        {
            return _roadMapRepository.GetByIdAsync(roadmapId, cancellationToken);
        }
    }
}
