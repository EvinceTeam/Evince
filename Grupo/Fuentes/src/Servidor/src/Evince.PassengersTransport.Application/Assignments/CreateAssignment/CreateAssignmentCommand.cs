﻿using MediatR;
using System;

namespace Evince.PassengersTransport.Application.Assignments.CreateAssignment
{
    public class CreateAssignmentCommand : IRequest
    {
        public string DriverId { get; set; }
        public int RoadMapId { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public TimeSpan StartTime { get; set; }
    }
}
