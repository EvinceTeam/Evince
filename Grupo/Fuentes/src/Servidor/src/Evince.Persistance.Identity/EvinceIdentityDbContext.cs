﻿using Evince.Application.Interfaces.Persistance;
using Evince.Persistance.Identity.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Evince.Persistance.Identity
{
    public class EvinceIdentityDbContext : IdentityDbContext<ApplicationUser>
    {
        public EvinceIdentityDbContext(DbContextOptions<EvinceIdentityDbContext> options)
            : base(options)
        {
        }

        public DbSet<RefreshToken> RefreshTokens { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            foreach (var entityType in modelBuilder.Model.GetEntityTypes())
            {
                var table = entityType.Relational().TableName;

                if (table.StartsWith("AspNet"))
                {
                    entityType.Relational().TableName = table.Substring(6);
                }
            }

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EvinceIdentityDbContext).Assembly);
        }
    }
}
