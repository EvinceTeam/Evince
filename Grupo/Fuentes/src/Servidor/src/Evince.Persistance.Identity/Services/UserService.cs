﻿using Evince.UsersManagement.Application.Models;
using Evince.UsersManagement.Application.Interfaces;
using Evince.Application.Interfaces.Models;
using Evince.Persistance.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Evince.Common.Entities;

namespace Evince.Persistance.Identity.Services
{
    public class UserService : UserManager<ApplicationUser>, IUserService
    {
        private readonly RoleManager<IdentityRole> _roleManager;

        public UserService(IUserStore<ApplicationUser> store,
            IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<ApplicationUser> passwordHasher,
            IEnumerable<IUserValidator<ApplicationUser>> userValidators,
            IEnumerable<IPasswordValidator<ApplicationUser>> passwordValidators,
            ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors,
            IServiceProvider services,
            ILogger<UserManager<ApplicationUser>> logger,
            RoleManager<IdentityRole> roleManager)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _roleManager = roleManager;
        }

        public async Task<IResult<bool>> ConfirmUserAsync(string userId, string token)
        {
            var user = await base.FindByIdAsync(userId);

            var confirmation = await base.ConfirmEmailAsync(user, token);

            if (!confirmation.Succeeded)
            {
                var error = confirmation.Errors.FirstOrDefault();

                base.Logger.LogError(error.Description, error);

                return new ServiceResult<bool> { Data = confirmation.Succeeded, Error = error.Description, Succeeded = false };
            }

            return new ServiceResult<bool> { Data = confirmation.Succeeded, Succeeded = true };
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(string userId)
        {
            var user = await base.FindByIdAsync(userId);

            return await base.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<UserDto> GetByIdAsync(string userId)
        {
            var user = await base.FindByIdAsync(userId);

            return MapApplicationUserToUserDto(user);
        }

        public async Task<UserDto> GetByNameAsync(string username)
        {
            var user = await base.FindByNameAsync(username);

            return MapApplicationUserToUserDto(user);
        }

        public async Task<IResult<string>> CreateAsync(string username, string password, string lastname, string name, Roles roleId)
        {
            var user = new ApplicationUser
            {
                EmailConfirmed = true,
                UserName = username,
                Lastname = lastname,
                Name = name
            };

            var registration = await base.CreateAsync(user, password);

            if (!registration.Succeeded)
            {
                var error = registration.Errors.FirstOrDefault();

                base.Logger.LogError(error.Description, error);

                return new ServiceResult<string> { Error = error.Description, Succeeded = false };
            }

            var selectedRole = await _roleManager
                .Roles
                .FirstOrDefaultAsync(r => r.Id == ((int)roleId).ToString());

            if (selectedRole == null)
            {
                var deleteResult = await DeleteUserAsync(username);

                var error = "El rol elegido no existe";

                base.Logger.LogError("El rol elegido no existe");

                return new ServiceResult<string> { Error = error, Succeeded = false };
            }

            var addUserToRole = await AddUserToRoleByNameAsync(username, selectedRole.Name);

            if (!addUserToRole.Succeeded)
            {
                base.Logger.LogError(addUserToRole.Error);

                return new ServiceResult<string> { Error = addUserToRole.Error, Succeeded = false };
            }

            return new ServiceResult<string> { Data = user.Id, Succeeded = true };
        }

        public async Task<IResult<bool>> AddUserToRoleByNameAsync(string username, string role)
        {
            var user = await base.FindByNameAsync(username);

            var result = await base.AddToRoleAsync(user, role);

            if (!result.Succeeded)
            {
                var error = result.Errors.FirstOrDefault();

                base.Logger.LogError(error.Description, error);

                return new ServiceResult<bool> { Data = result.Succeeded, Error = error.Description, Succeeded = false };
            }

            return new ServiceResult<bool> { Data = result.Succeeded, Succeeded = true };
        }

        public async Task<IResult<bool>> DeleteUserAsync(string username)
        {
            var user = await Users.SingleAsync(u => u.UserName.Equals(username));

            var result = await base.DeleteAsync(user);

            if (!result.Succeeded)
            {
                var error = result.Errors.FirstOrDefault();

                base.Logger.LogError(error.Description, error);

                return new ServiceResult<bool> { Data = result.Succeeded, Error = error.Description, Succeeded = false };
            }

            return new ServiceResult<bool> { Data = result.Succeeded, Succeeded = true };
        }

        public async Task<IResult<IEnumerable<RoleDto>>> GetUserRolesAsync()
        {
            var userRoles = await _roleManager
                .Roles
                .Select(r => new RoleDto { Id = Convert.ToInt32(r.Id), Name = r.Name })
                .ToListAsync();

            return new ServiceResult<IEnumerable<RoleDto>> { Data = userRoles, Succeeded = true };
        }

        private UserDto MapApplicationUserToUserDto(ApplicationUser applicationUser)
        {
            return new UserDto
            {
                Email = applicationUser.Email,
                Id = applicationUser.Id,
                PasswordHash = applicationUser.PasswordHash,
                PhoneNumber = applicationUser.PhoneNumber,
                UserName = applicationUser.UserName
            };
        }

        public async Task<IResult<RoleDto>> GetUserRoleAsync(string username)
        {
            var user = await Users.SingleAsync(u => u.UserName.Equals(username));

            var userRoles = await base.GetRolesAsync(user);

            userRoles = userRoles.Select(r => r.ToLower()).ToList();

            if (userRoles.Contains("admin"))
            {
                return new ServiceResult<RoleDto> { Data = new RoleDto { Id = (int)Roles.Admin, Name = "Admin" }, Succeeded = true };
            }

            if (userRoles.Contains("consultant"))
            {
                return new ServiceResult<RoleDto> { Data = new RoleDto { Id = (int)Roles.Consultant, Name = "Consultant" }, Succeeded = true };
            }

            if (userRoles.Contains("driver"))
            {
                return new ServiceResult<RoleDto> { Data = new RoleDto { Id = (int)Roles.Driver, Name = "Driver" }, Succeeded = true };
            }

            return new ServiceResult<RoleDto> { Data = null, Error = "No se ha encontrado el rol del usuario", Succeeded = false };
        }
    }
}
