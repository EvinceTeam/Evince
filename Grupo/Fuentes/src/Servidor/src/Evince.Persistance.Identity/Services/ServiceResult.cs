﻿using Evince.Application.Interfaces.Models;

namespace Evince.Persistance.Identity.Services
{
    public class ServiceResult<T> : IResult<T>
    {
        public bool Succeeded { get; set; }
        public T Data { get; set; }
        public string Error { get; set; }
    }
}
