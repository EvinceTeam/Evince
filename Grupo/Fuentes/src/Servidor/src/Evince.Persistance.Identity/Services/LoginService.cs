﻿using Evince.UsersManagement.Application.Models;
using Evince.UsersManagement.Application.Interfaces;
using Evince.Common.Settings;
using Evince.Persistance.Identity.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Evince.Persistance.Identity.Services
{
    public class LoginService : SignInManager<ApplicationUser>, ILoginService
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly EvinceIdentityDbContext _context;
        private readonly JwtSettings _jwtSettings;
        private readonly TokenValidationParameters _tokenValidationParameters;

        public LoginService(UserManager<ApplicationUser> userManager,
            IHttpContextAccessor contextAccessor,
            IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory,
            IOptions<IdentityOptions> optionsAccessor,
            ILogger<SignInManager<ApplicationUser>> logger,
            IAuthenticationSchemeProvider schemes,
            RoleManager<IdentityRole> roleManager,
            EvinceIdentityDbContext context,
            JwtSettings jwtSettings,
            TokenValidationParameters tokenValidationParameters)
            : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes)
        {
            _roleManager = roleManager;
            _context = context;
            _jwtSettings = jwtSettings;
            _tokenValidationParameters = tokenValidationParameters;
        }

        public async Task<AuthenticationResult> LoginAsync(string username, string password, bool remember)
        {
            var user = await UserManager.FindByNameAsync(username);

            if (user == null)
            {
                return new AuthenticationResult { Error = "El usuario ingresado no existe", Succeeded = false };
            }

            var login = await base.PasswordSignInAsync(username,
                    password,
                    remember,
                    lockoutOnFailure: false);

            if (!login.Succeeded)
            {
                base.Logger.LogInformation("Ha ocurrido un error al intentar ingresar al sistema");

                var error = "Usuario o contraseña incorrecta";

                if (login.IsNotAllowed)
                {
                    error = $"Usuario {username} no tiene permitido ingresar al sistema";

                    base.Logger.LogError(error);
                }

                if (login.IsLockedOut)
                {
                    error = $"Usuario {username} esta bloqueado";

                    base.Logger.LogError(error);
                }

                if (login.RequiresTwoFactor)
                {
                    error = $"Usuario {username} requiere confirmar su cuenta";

                    base.Logger.LogError(error);
                }

                base.Logger.LogError(error);

                return new AuthenticationResult { Error = error, Succeeded = false };
            }

            return await GenerateAuthenticationResultForUserAsync(user);
        }

        public Task LogoutAsync()
        {
            return base.SignOutAsync();
        }

        public async Task<AuthenticationResult> RefreshTokenAsync(string token, string refreshToken)
        {
            var validatedToken = GetPrincipalFromToken(token);

            if (validatedToken == null)
            {
                return new AuthenticationResult { Error = "Token inválido" };
            }

            var expiryDateUnix =
                long.Parse(validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Exp).Value);

            var expiryDateTimeUtc = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                .AddSeconds(expiryDateUnix);

            if (expiryDateTimeUtc > DateTime.UtcNow)
            {
                return new AuthenticationResult { Error = "El token aún no ha expirado" };
            }

            var jti = validatedToken.Claims.Single(x => x.Type == JwtRegisteredClaimNames.Jti).Value;

            var storedRefreshToken = await _context.RefreshTokens.SingleOrDefaultAsync(x => x.Token == refreshToken);

            if (storedRefreshToken == null)
            {
                return new AuthenticationResult { Error = "El refresh token no existe" };
            }

            if (DateTime.UtcNow > storedRefreshToken.ExpiryDate)
            {
                return new AuthenticationResult { Error = "El refresh token ha expirado" };
            }

            if (storedRefreshToken.Invalidated)
            {
                return new AuthenticationResult { Error = "El refresh token ha sido invalidado" };
            }

            if (storedRefreshToken.Used)
            {
                return new AuthenticationResult { Error = "El refresh token están en uso" };
            }

            if (storedRefreshToken.JwtId != jti)
            {
                return new AuthenticationResult { Error = "El refesh token no coincide con el JWT" };
            }

            storedRefreshToken.Used = true;

            _context.RefreshTokens.Update(storedRefreshToken);

            var user = await UserManager.FindByIdAsync(validatedToken.Claims.Single(x => x.Type == "id").Value);

            return await GenerateAuthenticationResultForUserAsync(user);
        }

        private ClaimsPrincipal GetPrincipalFromToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
                var tokenValidationParameters = _tokenValidationParameters.Clone();

                tokenValidationParameters.ValidateLifetime = false;

                var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var validatedToken);

                if (!IsJwtWithValidSecurityAlgorithm(validatedToken))
                {
                    return null;
                }

                return principal;
            }
            catch
            {
                return null;
            }
        }

        private bool IsJwtWithValidSecurityAlgorithm(SecurityToken validatedToken)
        {
            return (validatedToken is JwtSecurityToken jwtSecurityToken) &&
                   jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256,
                       StringComparison.InvariantCultureIgnoreCase);
        }

        private async Task<AuthenticationResult> GenerateAuthenticationResultForUserAsync(ApplicationUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, user.UserName),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim("id", user.Id)
            };

            var userClaims = await UserManager.GetClaimsAsync(user);

            claims.AddRange(userClaims);

            var userRoles = await UserManager.GetRolesAsync(user);

            foreach (var userRole in userRoles)
            {
                claims.Add(new Claim(ClaimTypes.Role, userRole));

                var role = await _roleManager.FindByNameAsync(userRole);

                if (role == null) continue;

                var roleClaims = await _roleManager.GetClaimsAsync(role);

                foreach (var roleClaim in roleClaims)
                {
                    if (claims.Contains(roleClaim))
                        continue;

                    claims.Add(roleClaim);
                }
            }

            var key = Encoding.ASCII.GetBytes(_jwtSettings.Secret);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.Add(_jwtSettings.TokenLifetime),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var refreshToken = new RefreshToken
            {
                JwtId = token.Id,
                UserId = user.Id,
                CreationDate = DateTime.UtcNow,
                ExpiryDate = DateTime.UtcNow.AddMonths(6)
            };

            await _context.RefreshTokens.AddAsync(refreshToken);
            await _context.SaveChangesAsync();

            return new AuthenticationResult
            {
                Succeeded = true,
                Token = tokenHandler.WriteToken(token),
                RefreshToken = refreshToken.Token
            };
        }
    }
}
