﻿using Microsoft.AspNetCore.Identity;

namespace Evince.Persistance.Identity.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}
