﻿using Evince.Common.Entities;
using Evince.UsersManagement.Application.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace Evince.Persistance.Identity
{
    public class EvinceIdentityInitializer
    {
        public static void Initialize(EvinceIdentityDbContext context, IUserService userService)
        {
            var initializer = new EvinceIdentityInitializer();
            initializer.SeedEverything(context, userService);
        }

        public void SeedEverything(EvinceIdentityDbContext context, IUserService userService)
        {
            context.Database.EnsureCreated();

            if (!context.Roles.Any())
            {
                SeedRoles(context);
            }

            if (!context.Users.Any(u => u.UserName.Equals("admin")))
            {
                SeedAdminUser(context, userService);
            }
        }

        private void SeedRoles(EvinceIdentityDbContext context)
        {
            var roles = new[]
            {
                new IdentityRole { Id = ((int)Roles.Admin).ToString(), Name = "Admin", NormalizedName = "ADMIN" },
                new IdentityRole { Id = ((int)Roles.Driver).ToString(), Name = "Driver", NormalizedName = "DRIVER" },
                new IdentityRole { Id = ((int)Roles.Consultant).ToString(), Name = "Consultant", NormalizedName = "CONSULTANT" },
            };

            context.Roles.AddRange(roles);

            context.SaveChanges();
        }

        private void SeedAdminUser(EvinceIdentityDbContext context, IUserService userService)
        {
            userService.CreateAsync("admin", "admin123", "admin", "admin", Roles.Admin).Wait();

            context.SaveChanges();
        }
    }
}
