﻿using Evince.Persistance.Identity.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Evince.Persistance.Identity
{
    public class EvinceIdentityDbContextFactory : DesignTimeDbContextFactoryBase<EvinceIdentityDbContext>
    {
        protected override EvinceIdentityDbContext CreateNewInstance(DbContextOptions<EvinceIdentityDbContext> options)
        {
            return new EvinceIdentityDbContext(options);
        }
    }
}
