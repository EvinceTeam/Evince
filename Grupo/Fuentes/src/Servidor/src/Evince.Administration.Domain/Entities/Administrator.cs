﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using Evince.PassengersTransport.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.Administration.Domain.Entities
{
    public class Administrator : BaseEntity<string>
    {
        public Administrator(string id, string name, string lastname)
        {
            ValidateInvariants(id, name, lastname);

            Id = id;
            Name = name;
            Lastname = lastname;

            Subscriptions = new List<Subscription>();
        }

        public string Name { get; private set; }
        public string Lastname { get; private set; }

        public string Fullname { get { return $"{Lastname} {Name}"; } }

        public IList<Subscription> Subscriptions { get; private set; }

        public Administrator AddSubscription(Subscription subscription)
        {
            Guard.Against.Null(subscription, nameof(subscription));

            if (Subscriptions.Any(s => s.RoadMap.Id == subscription.RoadMap.Id))
            {
                throw new InvalidOperationException("El administrador ya se encuentra suscrito al recorrido");
            }

            Subscriptions.Add(subscription);

            return this;
        }

        public Administrator RemoveSubscription(Subscription subscription)
        {
            Guard.Against.Null(subscription, nameof(subscription));

            if (!Subscriptions.Any(s => s.Id == subscription.Id))
            {
                throw new InvalidOperationException("No se pudo encontrar la suscripcion");
            }

            Subscriptions.Remove(subscription);

            return this;
        }

        public Administrator RemoveSubscription(int subscriptionId)
        {
            Guard.Against.Zero(subscriptionId, nameof(subscriptionId));

            if (!Subscriptions.Any(s => s.Id == subscriptionId))
            {
                throw new InvalidOperationException("No se pudo encontrar la suscripcion");
            }

            return RemoveSubscription(Subscriptions.Single(s => s.Id == subscriptionId));
        }

        public Administrator RemoveSubscription(RoadMap roadmap)
        {
            Guard.Against.Null(roadmap, nameof(roadmap));

            if (!Subscriptions.Any(s => s.RoadMap.Id == roadmap.Id))
            {
                throw new InvalidOperationException("No se pudo encontrar la suscripcion");
            }

            return RemoveSubscription(Subscriptions.Single(s => s.RoadMap.Id == roadmap.Id));
        }

        private void ValidateInvariants(string id, string name, string lastname)
        {
            Guard.Against.NullOrWhiteSpace(id, nameof(id));
            Guard.Against.NullOrWhiteSpace(name, nameof(name));
            Guard.Against.NullOrWhiteSpace(lastname, nameof(lastname));
        }
    }
}
