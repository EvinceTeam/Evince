﻿using Evince.Common.Entities;
using Evince.Common.Guards;
using Evince.PassengersTransport.Domain.Entities;
using System;

namespace Evince.Administration.Domain.Entities
{
    public class Subscription : BaseEntity<int>
    {
        public Subscription(string administratorId, RoadMap roadmap)
        {
            ValidateInvariants(administratorId, roadmap);

            AdministratorId = administratorId;
            RoadMap = roadmap;
        }

        public string AdministratorId { get; private set; }
        public int RoadMapId { get; private set; }
        public RoadMap RoadMap { get; private set; }

        protected Subscription() { }

        private void ValidateInvariants(string administratorId, RoadMap roadmap)
        {
            Guard.Against.NullOrWhiteSpace(administratorId, nameof(administratorId));
            Guard.Against.Null(roadmap, nameof(roadmap));
        }
    }
}
