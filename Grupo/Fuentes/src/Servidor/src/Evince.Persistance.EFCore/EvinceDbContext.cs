﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance;
using Evince.PassengersTransport.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Evince.Persistance.EFCore
{
    public class EvinceDbContext : DbContext, IEvinceDbContext
    {
        public EvinceDbContext(DbContextOptions<EvinceDbContext> options)
            : base(options)
        {
        }

        public DbSet<Terminal> Terminals { get; set; }
        public DbSet<Travel> Travels { get; set; }
        public DbSet<Bus> Buses { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<RoadMap> RoadMaps { get; set; }
        public DbSet<Journey> Journeys { get; set; }
        public DbSet<Delay> Delays { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<DriverAssignment> DriverAssignments { get; set; }

        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EvinceDbContext).Assembly);

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Driver>()
                .Property(s => s.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<Bus>()
                .Property(s => s.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<Terminal>()
                .Property(s => s.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<Administrator>()
                .Property(s => s.Id)
                .ValueGeneratedNever();
        }
    }
}
