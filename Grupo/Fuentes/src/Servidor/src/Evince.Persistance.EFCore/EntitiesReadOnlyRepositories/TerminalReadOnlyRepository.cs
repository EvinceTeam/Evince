﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class TerminalReadOnlyRepository : EfReadOnlyRepository<Terminal, int>, ITerminalReadOnlyRepository
    {
        public TerminalReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
