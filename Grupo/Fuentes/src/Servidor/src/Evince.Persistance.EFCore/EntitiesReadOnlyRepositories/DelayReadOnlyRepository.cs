﻿using Evince.PassengersTransport.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class DelayReadOnlyRepository : EfReadOnlyRepository<Delay, int>, IDelayReadOnlyRepository
    {
        public DelayReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
