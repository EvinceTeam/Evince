﻿using Evince.PassengersTransport.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class BusReadOnlyRepository : EfReadOnlyRepository<Bus, string>, IBusReadOnlyRepository
    {
        public BusReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
