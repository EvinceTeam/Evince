﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class SubscriptionReadOnlyRepository : EfReadOnlyRepository<Subscription, int>, ISubscriptionReadOnlyRepository
    {
        public SubscriptionReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
