﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class DriverReadOnlyRepository : EfReadOnlyRepository<Driver, string>, IDriverReadOnlyRepository
    {
        public DriverReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
