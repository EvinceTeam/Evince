﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class AdministratorReadOnlyRepository : EfReadOnlyRepository<Administrator, string>, IAdministratorReadOnlyRepository
    {
        public AdministratorReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
