﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class RoadMapItemReadOnlyRepository : EfReadOnlyRepository<RoadMapItem, int>, IRoadMapItemReadOnlyRepository
    {
        public RoadMapItemReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
