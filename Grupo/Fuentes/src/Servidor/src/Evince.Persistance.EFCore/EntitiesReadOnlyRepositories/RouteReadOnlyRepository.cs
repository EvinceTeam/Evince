﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class RouteReadOnlyRepository : EfReadOnlyRepository<Route, int>, IRouteReadOnlyRepository
    {
        public RouteReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
