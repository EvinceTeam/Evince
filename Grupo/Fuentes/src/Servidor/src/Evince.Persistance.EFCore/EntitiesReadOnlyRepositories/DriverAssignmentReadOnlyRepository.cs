﻿using Evince.PassengersTransport.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class DriverAssignmentReadOnlyRepository : EfReadOnlyRepository<DriverAssignment, int>, IDriverAssignmentReadOnlyRepository
    {
        public DriverAssignmentReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
