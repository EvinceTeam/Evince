﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class RoadMapReadOnlyRepository : EfReadOnlyRepository<RoadMap, int>, IRoadMapReadOnlyRepository
    {
        public RoadMapReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
