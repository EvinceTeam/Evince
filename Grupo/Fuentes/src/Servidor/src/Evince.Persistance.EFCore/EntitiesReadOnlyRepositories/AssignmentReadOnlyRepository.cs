﻿using Evince.PassengersTransport.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class AssignmentReadOnlyRepository : EfReadOnlyRepository<Assignment, int>, IAssignmentReadOnlyRepository
    {
        public AssignmentReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
