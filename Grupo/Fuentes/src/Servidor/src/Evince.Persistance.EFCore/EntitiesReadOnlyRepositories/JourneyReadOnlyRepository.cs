﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class JourneyReadOnlyRepository : EfReadOnlyRepository<Journey, int>, IJourneyReadOnlyRepository
    {
        public JourneyReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
