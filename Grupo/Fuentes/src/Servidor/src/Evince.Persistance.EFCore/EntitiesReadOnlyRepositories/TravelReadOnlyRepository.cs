﻿using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesReadOnlyRepositories
{
    public class TravelReadOnlyRepository : EfReadOnlyRepository<Travel, int>, ITravelReadOnlyRepository
    {
        public TravelReadOnlyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
