﻿using Evince.Application.Interfaces.Persistance;
using Evince.Application.Specifications;
using Evince.Common.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Persistance.EFCore
{
    public class EfReadOnlyRepository<T, TKey> : IEfReadOnlyRepository<T, TKey> where T : BaseEntity<TKey>
    {
        protected readonly EvinceDbContext _context;

        public EfReadOnlyRepository(EvinceDbContext context)
        {
            _context = context;
        }

        public T GetById(TKey id)
        {
            return _context.Set<T>().AsNoTracking().FirstOrDefault(e => e.Id.Equals(id));
        }

        public Task<T> GetByIdAsync(TKey id, CancellationToken cancellationToken = default)
        {
            return _context.Set<T>().AsNoTracking().FirstOrDefaultAsync(e => e.Id.Equals(id), cancellationToken);
        }

        public T GetSingleBySpec(ISpecification<T> spec)
        {
            return List(spec).FirstOrDefault();
        }

        public async Task<T> GetSingleBySpecAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var entities = await ListAsync(spec, cancellationToken);

            return entities.FirstOrDefault();
        }

        public IEnumerable<T> ListAll()
        {
            return _context.Set<T>().AsNoTracking().AsEnumerable();
        }

        public async Task<IEnumerable<T>> ListAllAsync(CancellationToken cancellationToken = default)
        {
            var entities = await _context.Set<T>().AsNoTracking().ToListAsync(cancellationToken);

            return entities.AsEnumerable();
        }

        public IEnumerable<T> List(ISpecification<T> spec)
        {
            var entities = ApplySpecification(spec).AsNoTracking().AsEnumerable();

            return entities;
        }

        public async Task<IEnumerable<T>> ListAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var entities = await ApplySpecification(spec).AsNoTracking().ToListAsync();

            return entities;
        }

        public int Count(ISpecification<T> spec)
        {
            return ApplySpecification(spec).AsNoTracking().Count();
        }

        public Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            return ApplySpecification(spec).AsNoTracking().CountAsync(cancellationToken);
        }

        private IQueryable<T> ApplySpecification(ISpecification<T> spec)
        {
            return SpecificationEvaluator<T, TKey>.GetQuery(_context.Set<T>().AsQueryable(), spec);
        }
    }
}
