﻿using Evince.Persistance.EFCore.Infrastructure;
using Microsoft.EntityFrameworkCore;

namespace Evince.Persistance.EFCore
{
    public class EvinceDbContextFactory : DesignTimeDbContextFactoryBase<EvinceDbContext>
    {
        protected override EvinceDbContext CreateNewInstance(DbContextOptions<EvinceDbContext> options)
        {
            return new EvinceDbContext(options);
        }
    }
}
