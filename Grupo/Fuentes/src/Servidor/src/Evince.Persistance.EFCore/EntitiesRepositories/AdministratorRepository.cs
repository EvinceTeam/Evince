﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesRepositories;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class AdministratorRepository : EfRepository<Administrator, string>, IAdministratorRepository
    {
        public AdministratorRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
