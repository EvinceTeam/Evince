﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class DelayRepository : EfRepository<Delay, int>, IDelayRepository
    {
        public DelayRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
