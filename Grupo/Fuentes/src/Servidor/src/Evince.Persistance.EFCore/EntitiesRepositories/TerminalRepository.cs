﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class TerminalRepository : EfRepository<Terminal, int>, ITerminalRepository
    {
        public TerminalRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
