﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class RoadMapRepository : EfRepository<RoadMap, int>, IRoadMapRepository
    {
        public RoadMapRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
