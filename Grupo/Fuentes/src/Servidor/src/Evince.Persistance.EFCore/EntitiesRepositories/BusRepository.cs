﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class BusRepository : EfRepository<Bus, string>, IBusRepository
    {
        public BusRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
