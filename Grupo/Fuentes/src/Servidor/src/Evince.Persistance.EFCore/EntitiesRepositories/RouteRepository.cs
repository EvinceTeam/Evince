﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class RouteRepository : EfRepository<Route, int>, IRouteRepository
    {
        public RouteRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
