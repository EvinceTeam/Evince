﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class DriverRepository : EfRepository<Driver, string>, IDriverRepository
    {
        public DriverRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
