﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class DriverAssignmentRepository : EfRepository<DriverAssignment, int>, IDriverAssignmentRepository
    {
        public DriverAssignmentRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
