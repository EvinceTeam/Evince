﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class AssignmentRepository : EfRepository<Assignment, int>, IAssignmentRepository
    {
        public AssignmentRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
