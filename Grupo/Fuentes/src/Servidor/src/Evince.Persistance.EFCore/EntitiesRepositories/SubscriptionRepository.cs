﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesRepositories;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class SubscriptionRepository : EfRepository<Subscription, int>, ISubscriptionRepository
    {
        public SubscriptionRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
