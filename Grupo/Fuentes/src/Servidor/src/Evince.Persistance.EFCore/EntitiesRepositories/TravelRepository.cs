﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class TravelRepository : EfRepository<Travel, int>, ITravelRepository
    {
        public TravelRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
