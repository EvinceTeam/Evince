﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class JourneyRepository : EfRepository<Journey, int>, IJourneyRepository
    {
        public JourneyRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
