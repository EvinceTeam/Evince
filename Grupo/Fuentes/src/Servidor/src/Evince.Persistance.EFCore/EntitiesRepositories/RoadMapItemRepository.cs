﻿using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.PassengersTransport.Domain.Entities;

namespace Evince.Persistance.EFCore.EntitiesRepositories
{
    public class RoadMapItemRepository : EfRepository<RoadMapItem, int>, IRoadMapItemRepository
    {
        public RoadMapItemRepository(EvinceDbContext context) : base(context)
        {
        }
    }
}
