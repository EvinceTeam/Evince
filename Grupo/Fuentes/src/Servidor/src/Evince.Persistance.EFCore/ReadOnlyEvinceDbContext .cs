﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance;
using Evince.PassengersTransport.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Evince.Persistance.EFCore
{
    public class ReadOnlyEvinceDbContext : IReadOnlyEvinceDbContext
    {
        private readonly IEvinceDbContext _dbContext;

        public ReadOnlyEvinceDbContext(IEvinceDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IQueryable<Terminal> Terminals
        {
            get
            {
                return _dbContext.Terminals.AsNoTracking();
            }
        }

        public IQueryable<Bus> Buses
        {
            get
            {
                return _dbContext.Buses.AsNoTracking();
            }
        }

        public IQueryable<Driver> Drivers
        {
            get
            {
                return _dbContext.Drivers.AsNoTracking();
            }
        }

        public IQueryable<Travel> Travels
        {
            get
            {
                return _dbContext.Travels.AsNoTracking();
            }
        }

        public IQueryable<RoadMap> RoadMaps
        {
            get
            {
                return _dbContext.RoadMaps.AsNoTracking();
            }
        }

        public IQueryable<Journey> Journeys
        {
            get
            {
                return _dbContext.Journeys.AsNoTracking();
            }
        }

        public IQueryable<Subscription> Subscriptions
        {
            get
            {
                return _dbContext.Subscriptions.AsNoTracking();
            }
        }
    }
}
