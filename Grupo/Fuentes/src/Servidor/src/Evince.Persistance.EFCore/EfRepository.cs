﻿using Evince.Application.Interfaces.Persistance;
using Evince.Application.Specifications;
using Evince.Common.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Persistance.EFCore
{
    public class EfRepository<T, TKey> : IEfRepository<T, TKey> where T : BaseEntity<TKey>
    {
        protected readonly EvinceDbContext _context;

        public EfRepository(EvinceDbContext context)
        {
            _context = context;
        }

        public T GetById(TKey id)
        {
            return _context.Set<T>().Find(id);
        }

        public Task<T> GetByIdAsync(TKey id, CancellationToken cancellationToken = default)
        {
            return _context.Set<T>().FindAsync(id);
        }

        public T GetSingleBySpec(ISpecification<T> spec)
        {
            return List(spec).FirstOrDefault();
        }

        public async Task<T> GetSingleBySpecAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var entities = await ListAsync(spec, cancellationToken);

            return entities.FirstOrDefault();
        }

        public IEnumerable<T> ListAll()
        {
            return _context.Set<T>().AsEnumerable();
        }

        public async Task<IEnumerable<T>> ListAllAsync(CancellationToken cancellationToken = default)
        {
            var entities = await _context.Set<T>().ToListAsync(cancellationToken);

            return entities.AsEnumerable();
        }

        public IEnumerable<T> List(ISpecification<T> spec)
        {
            var entities = ApplySpecification(spec).AsEnumerable();

            return entities;
        }

        public async Task<IEnumerable<T>> ListAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            var entities = await ApplySpecification(spec).ToListAsync(cancellationToken);

            return entities;
        }

        public int Count(ISpecification<T> spec)
        {
            return ApplySpecification(spec).Count();
        }

        public Task<int> CountAsync(ISpecification<T> spec, CancellationToken cancellationToken = default)
        {
            return ApplySpecification(spec).CountAsync(cancellationToken);
        }

        public T Add(T entity)
        {
            _context.Set<T>().Add(entity);

            SaveChanges();

            return entity;
        }

        public async Task<T> AddAsync(T entity, CancellationToken cancellationToken = default)
        {
            await _context.Set<T>().AddAsync(entity, cancellationToken);

            await SaveChangesAsync();

            return entity;
        }

        public void Update(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;

            SaveChanges();
        }

        public Task UpdateAsync(T entity, CancellationToken cancellationToken = default)
        {
            _context.Entry(entity).State = EntityState.Modified;

            return SaveChangesAsync(cancellationToken);
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);

            SaveChanges();
        }

        public Task DeleteAsync(T entity, CancellationToken cancellationToken = default)
        {
            _context.Set<T>().Remove(entity);

            return _context.SaveChangesAsync(cancellationToken);
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return _context.SaveChangesAsync(cancellationToken);
        }

        private IQueryable<T> ApplySpecification(ISpecification<T> spec)
        {
            return SpecificationEvaluator<T, TKey>.GetQuery(_context.Set<T>().AsQueryable(), spec);
        }
    }
}
