﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Administration.Application.Administrators.Queries.GetRoadMapsByNodes
{
    public class GetRoadMapsByNodesQueryHandler : IRequestHandler<GetRoadMapsByNodesQuery, IEnumerable<RoadMapDto>>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;
        private readonly ISubscriptionReadOnlyRepository _subscriptionRepository;

        public GetRoadMapsByNodesQueryHandler(ITravelReadOnlyRepository travelRepository, ISubscriptionReadOnlyRepository subscriptionRepository)
        {
            _travelRepository = travelRepository;
            _subscriptionRepository = subscriptionRepository;
        }

        public async Task<IEnumerable<RoadMapDto>> Handle(GetRoadMapsByNodesQuery request, CancellationToken cancellationToken)
        {
            var driverTravels = await GetTravelsAsync(request.StartNode, request.EndNode, cancellationToken);

            if (!driverTravels.Any())
            {
                return Enumerable.Empty<RoadMapDto>();
            }

            var subsciptions = await GetAdministratorSubscriptionsAsync(request.AdministratorId, cancellationToken);

            var driverRoadmaps = driverTravels.GroupBy(t => t.RoadMap)
                .ToDictionary(g => g.Key, g => g.Select(t => t).OrderBy(t => t.RoadmapTravel.Order).AsEnumerable());

            return GetRoadMapsByNodesQueryDto.MapFrom(driverRoadmaps, subsciptions);
        }

        private async Task<IEnumerable<Travel>> GetTravelsAsync(int? startNode, int? endNode, CancellationToken cancellationToken)
        {
            var travels = await _travelRepository.ListAsync(new TravelsWithDelaysRoadMapJourneysSpecification(new DateTime(2017, 1, 1)), cancellationToken);

            if (startNode.HasValue)
            {
                travels = travels.Where(t => t.RoadMap.FromId == startNode);
            }

            if (endNode.HasValue)
            {
                travels = travels.Where(t => t.RoadMap.ToId == endNode);
            }

            return travels;
        }

        private Task<IEnumerable<Subscription>> GetAdministratorSubscriptionsAsync(string administratorId, CancellationToken cancellationToken)
        {
            return _subscriptionRepository.ListAsync(new SubscriptionsWithRoadMapSpecification(administratorId), cancellationToken);
        }
    }
}
