﻿using Evince.Administration.Domain.Entities;
using Evince.PassengersTransport.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMap
{
    public class GetAdministratorSuscribedRoadMapQueryDto
    {
        public static RoadMapDto MapFrom(KeyValuePair<RoadMap, IEnumerable<Travel>> source, IEnumerable<Subscription> administratorSubscriptions)
        {
            var adminSubscribed = administratorSubscriptions.Any(s => s.RoadMap.Id == source.Key.Id);

            return RoadMapDto.MapFrom(source.Key, source.Value, adminSubscribed);
        }
    }

    public class RoadMapDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Nombre { get; set; }
        public string Origen { get; set; }
        public string OrigenCompleto { get; set; }
        public int? OrigenId { get; set; }
        public string OrigenCodigo { get; set; }
        public string OrigenProvincia { get; set; }
        public string OrigenPais { get; set; }
        public string Destino { get; set; }
        public string DestinoCompleto { get; set; }
        public int? DestinoId { get; set; }
        public string DestinoCodigo { get; set; }
        public string DestinoProvincia { get; set; }
        public string DestinoPais { get; set; }
        public bool Subscription { get; set; }
        public IEnumerable<TravelDto> Viajes { get; set; }

        public static RoadMapDto MapFrom(RoadMap roadmap, IEnumerable<Travel> travels, bool subscribed)
        {
            return new RoadMapDto
            {
                Id = roadmap.Id,
                Code = $"{roadmap.FromCode}-{roadmap.ToCode}",
                Nombre = $"{roadmap.From} - {roadmap.To}",
                Origen = roadmap.FromShort,
                OrigenId = roadmap.FromId,
                OrigenCodigo = roadmap.FromCode,
                OrigenCompleto = roadmap.From,
                OrigenPais = roadmap.FromCountry,
                OrigenProvincia = roadmap.FromProvince,
                Destino = roadmap.ToShort,
                DestinoId = roadmap.ToId,
                DestinoCodigo = roadmap.ToCode,
                DestinoCompleto = roadmap.To,
                DestinoPais = roadmap.ToCountry,
                DestinoProvincia = roadmap.ToProvince,
                Subscription = subscribed,
                Viajes = travels.Select(travel => TravelDto.MapFrom(travel)),
            };
        }
    }

    public class TravelDto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string NombreCorto { get; set; }
        public int CantPasajeros { get; set; }
        public int CantButacasColectivo { get; set; }
        public DateTime FechaHoraSalidaEstipuladas { get; set; }
        public DateTime FechaHoraLlegadaEstipuladas { get; set; }
        public JourneyDto Trayecto { get; set; }
        public string Estado { get; set; }
        public int Orden { get; set; }
        public IEnumerable<DelayDto> Retrasos { get; set; }

        public static TravelDto MapFrom(Travel source)
        {
            string estado = string.Empty;

            switch (source.State)
            {
                case TravelState.Pending:
                    estado = "Pendiente";
                    break;
                case TravelState.InProgress:
                    estado = "En camino";
                    break;
                case TravelState.Finalized:
                    estado = "Finalizado";
                    break;
                default:
                    estado = "Pendiente";
                    break;
            }

            return new TravelDto
            {
                Id = source.Id,
                Nombre = source.Name,
                NombreCorto = $"{source.FromCode}-{source.ToCode}",
                CantPasajeros = source.NumberOfPassengers,
                CantButacasColectivo = source.Bus.Capacity,
                FechaHoraSalidaEstipuladas = source.StipulatedStartTime,
                FechaHoraLlegadaEstipuladas = source.StipulatedEndTime,
                Trayecto = JourneyDto.MapFrom(source.Journey),
                Estado = estado,
                Orden = source.RoadmapTravel.Order,
                Retrasos = source.Delays.Select(d => DelayDto.MapFrom(d, source.StipulatedStartTime, source.StipulatedEndTime))
            };
        }
    }

    public class JourneyDto
    {
        public int Id { get; set; }
        public string TerminalOrigen { get; set; }
        public string TerminalOrigenCodigo { get; set; }
        public int TerminalOrigenId { get; set; }
        public string TerminalDestino { get; set; }
        public string TerminalDestinoCodigo { get; set; }
        public int TerminalDestinoId { get; set; }

        public static JourneyDto MapFrom(Journey source)
        {
            return new JourneyDto
            {
                Id = source.Id,
                TerminalOrigen = source.StartNode.ShortDescription,
                TerminalOrigenCodigo = source.StartNode.Code,
                TerminalOrigenId = source.StartNode.Id,
                TerminalDestino = source.EndNode.ShortDescription,
                TerminalDestinoCodigo = source.EndNode.Code,
                TerminalDestinoId = source.EndNode.Id
            };
        }
    }

    public class DelayDto
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public double Tiempo { get; set; }
        public DateTime FechaHoraSalidaEstipuladas { get; set; }
        public DateTime FechaHoraLlegadaEstipuladas { get; set; }

        public static DelayDto MapFrom(Delay source, DateTime startTime, DateTime endTime)
        {
            return new DelayDto
            {
                Id = source.Id,
                Tipo = source.Type.ToFriendlyString(),
                Descripcion = source.Description,
                Tiempo = source.Time.TotalMinutes,
                FechaHoraSalidaEstipuladas = startTime,
                FechaHoraLlegadaEstipuladas = endTime
            };
        }
    }
}
