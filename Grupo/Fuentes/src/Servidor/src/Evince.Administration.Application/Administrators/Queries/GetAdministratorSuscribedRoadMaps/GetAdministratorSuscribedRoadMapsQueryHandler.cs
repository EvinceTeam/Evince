﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorSuscribedRoadMaps
{
    public class GetAdministratorSuscribedRoadMapsQueryHandler : IRequestHandler<GetAdministratorSuscribedRoadMapsQuery, IEnumerable<RoadMapDto>>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;
        private readonly ISubscriptionReadOnlyRepository _subscriptionRepository;

        public GetAdministratorSuscribedRoadMapsQueryHandler(ITravelReadOnlyRepository travelRepository, ISubscriptionReadOnlyRepository subscriptionRepository)
        {
            _travelRepository = travelRepository;
            _subscriptionRepository = subscriptionRepository;
        }

        public async Task<IEnumerable<RoadMapDto>> Handle(GetAdministratorSuscribedRoadMapsQuery request, CancellationToken cancellationToken)
        {
            var driverTravels = await GetTravelsAsync(cancellationToken);

            if (!driverTravels.Any())
            {
                return Enumerable.Empty<RoadMapDto>();
            }

            var subsciptions = await GetAdministratorSubscriptionsAsync(request.AdministratorId, cancellationToken);

            if (!subsciptions.Any())
            {
                return Enumerable.Empty<RoadMapDto>();
            }

            var driverRoadmaps = driverTravels.GroupBy(t => t.RoadMap)
                .Where(g => subsciptions.Any(s => s.RoadMap.Id == g.Key.Id))
                .ToDictionary(g => g.Key, g => g.Select(t => t).OrderBy(t => t.RoadmapTravel.Order).AsEnumerable());

            return GetAdministratorSuscribedRoadMapsQueryDto.MapFrom(driverRoadmaps, subsciptions);
        }

        private Task<IEnumerable<Travel>> GetTravelsAsync(CancellationToken cancellationToken)
        {
            return _travelRepository.ListAsync(new TravelsWithDelaysRoadMapJourneysSpecification(new DateTime(2017, 1, 1)), cancellationToken);
        }

        private Task<IEnumerable<Subscription>> GetAdministratorSubscriptionsAsync(string administratorId, CancellationToken cancellationToken)
        {
            return _subscriptionRepository.ListAsync(new SubscriptionsWithRoadMapSpecification(administratorId), cancellationToken);
        }
    }
}
