﻿using FluentValidation;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorSuscribedRoadMaps
{
    class GetAdministratorSuscribedRoadMapsQueryValidator : AbstractValidator<GetAdministratorSuscribedRoadMapsQuery>
    {
        public GetAdministratorSuscribedRoadMapsQueryValidator()
        {
            RuleFor(x => x.AdministratorId)
                .NotNull().WithMessage("El identificador del administrador no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del administrador no puede estar vacío");
        }
    }
}
