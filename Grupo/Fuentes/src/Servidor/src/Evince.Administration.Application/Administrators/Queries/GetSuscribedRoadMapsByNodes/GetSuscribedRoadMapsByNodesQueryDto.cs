﻿using Evince.Administration.Domain.Entities;
using Evince.PassengersTransport.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Evince.Administration.Application.Administrators.Queries.GetSuscribedRoadMapsByNodes
{
    public class GetSuscribedRoadMapsByNodesQueryDto
    {
        public static IEnumerable<RoadMapDto> MapFrom(IDictionary<RoadMap, IEnumerable<Travel>> source, IEnumerable<Subscription> administratorSubscriptions)
        {
            var recorridos = new List<RoadMapDto>();

            var order = 0;

            foreach (var roadmap in source.Keys)
            {
                var adminSubscribed = administratorSubscriptions.Any(s => s.RoadMap.Id == roadmap.Id);

                recorridos.Add(RoadMapDto.MapFrom(roadmap, source[roadmap], adminSubscribed));

                order++;
            }

            return recorridos;
        }
    }

    public class RoadMapDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Nombre { get; set; }
        public string Origen { get; set; }
        public string OrigenCompleto { get; set; }
        public int? OrigenId { get; set; }
        public string OrigenCodigo { get; set; }
        public string OrigenProvincia { get; set; }
        public string OrigenPais { get; set; }
        public string Destino { get; set; }
        public string DestinoCompleto { get; set; }
        public int? DestinoId { get; set; }
        public string DestinoCodigo { get; set; }
        public string DestinoProvincia { get; set; }
        public string DestinoPais { get; set; }
        public bool Subscription { get; set; }
        public IEnumerable<DelayDto> Retrasos { get; set; }

        public static RoadMapDto MapFrom(RoadMap roadmap, IEnumerable<Travel> travels, bool subscribed)
        {
            var delays = new List<DelayDto>();

            foreach (var travel in travels)
            {
                delays.AddRange(travel.Delays.Select(d => DelayDto.MapFrom(d, travel.StipulatedStartTime, travel.StipulatedEndTime)));
            }

            return new RoadMapDto
            {
                Id = roadmap.Id,
                Code = $"{roadmap.FromCode}-{roadmap.ToCode}",
                Nombre = $"{roadmap.From} - {roadmap.To}",
                Origen = roadmap.FromShort,
                OrigenId = roadmap.FromId,
                OrigenCodigo = roadmap.FromCode,
                OrigenCompleto = roadmap.From,
                OrigenPais = roadmap.FromCountry,
                OrigenProvincia = roadmap.FromProvince,
                Destino = roadmap.ToShort,
                DestinoId = roadmap.ToId,
                DestinoCodigo = roadmap.ToCode,
                DestinoCompleto = roadmap.To,
                DestinoPais = roadmap.ToCountry,
                DestinoProvincia = roadmap.ToProvince,
                Subscription = subscribed,
                Retrasos = delays
            };
        }
    }

    public class DelayDto
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Descripcion { get; set; }
        public double Tiempo { get; set; }
        public DateTime FechaHoraSalidaEstipuladas { get; set; }
        public DateTime FechaHoraLlegadaEstipuladas { get; set; }

        public static DelayDto MapFrom(Delay source, DateTime startTime, DateTime endTime)
        {
            return new DelayDto
            {
                Id = source.Id,
                Tipo = source.Type.ToFriendlyString(),
                Descripcion = source.Description,
                Tiempo = source.Time.TotalMinutes,
                FechaHoraSalidaEstipuladas = startTime,
                FechaHoraLlegadaEstipuladas = endTime
            };
        }
    }
}
