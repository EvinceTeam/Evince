﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesReadOnlyRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMap
{
    public class GetAdministratorRoadMapQueryHandler : IRequestHandler<GetAdministratorRoadMapQuery, RoadMapDto>
    {
        private readonly ITravelReadOnlyRepository _travelRepository;
        private readonly ISubscriptionReadOnlyRepository _subscriptionRepository;

        public GetAdministratorRoadMapQueryHandler(ITravelReadOnlyRepository travelRepository, ISubscriptionReadOnlyRepository subscriptionRepository)
        {
            _travelRepository = travelRepository;
            _subscriptionRepository = subscriptionRepository;
        }

        public async Task<RoadMapDto> Handle(GetAdministratorRoadMapQuery request, CancellationToken cancellationToken)
        {
            var driverTravels = await GetRoadMapTravelsAsync(request.RoadMapId, cancellationToken);

            if (!driverTravels.Any())
            {
                throw new InvalidOperationException("No se han encontrado viajes para el recorrido seleccionado");
            }

            var driverRoadmap = driverTravels.GroupBy(t => t.RoadMap)
                .ToDictionary(g => g.Key, g => g.Select(t => t).OrderBy(t => t.RoadmapTravel.Order).AsEnumerable())
                .FirstOrDefault();

            var subsciptions = await GetAdministratorSubscriptionsAsync(request.AdministratorId, cancellationToken);

            return GetAdministratorSuscribedRoadMapQueryDto.MapFrom(driverRoadmap, subsciptions);
        }

        private Task<IEnumerable<Travel>> GetRoadMapTravelsAsync(int roadmapId, CancellationToken cancellationToken)
        {
            return _travelRepository.ListAsync(new TravelsWithDelaysRoadMapJourneysSpecification(roadmapId, new DateTime(2017, 1, 1)), cancellationToken);
        }

        private Task<IEnumerable<Subscription>> GetAdministratorSubscriptionsAsync(string administratorId, CancellationToken cancellationToken)
        {
            return _subscriptionRepository.ListAsync(new SubscriptionsWithRoadMapSpecification(administratorId), cancellationToken);
        }
    }
}
