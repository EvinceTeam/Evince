﻿using FluentValidation;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMap
{
    class GetAdministratorRoadMapQueryValidator : AbstractValidator<GetAdministratorRoadMapQuery>
    {
        public GetAdministratorRoadMapQueryValidator()
        {
            RuleFor(x => x.AdministratorId)
                .NotNull().WithMessage("El identificador del administrador no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del administrador no puede estar vacío");
            RuleFor(x => x.RoadMapId)
                .NotNull().WithMessage("El identificador del recorrido no puede tener un valor nulo");
        }
    }
}
