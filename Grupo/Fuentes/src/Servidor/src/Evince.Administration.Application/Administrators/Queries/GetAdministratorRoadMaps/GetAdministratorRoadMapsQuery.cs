﻿using MediatR;
using System.Collections.Generic;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMaps
{
    public class GetAdministratorRoadMapsQuery : IRequest<IEnumerable<RoadMapDto>>
    {
        public string AdministratorId { get; set; }
    }
}
