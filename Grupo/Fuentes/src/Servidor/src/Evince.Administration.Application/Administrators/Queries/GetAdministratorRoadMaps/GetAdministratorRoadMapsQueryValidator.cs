﻿using FluentValidation;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMaps
{
    class GetAdministratorRoadMapsQueryValidator : AbstractValidator<GetAdministratorRoadMapsQuery>
    {
        public GetAdministratorRoadMapsQueryValidator()
        {
            RuleFor(x => x.AdministratorId)
                .NotNull().WithMessage("El identificador del administrador no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del administrador no puede estar vacío");
        }
    }
}
