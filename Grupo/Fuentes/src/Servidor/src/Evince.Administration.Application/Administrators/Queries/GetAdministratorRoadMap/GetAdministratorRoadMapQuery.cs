﻿using MediatR;
using System.Collections.Generic;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorRoadMap
{
    public class GetAdministratorRoadMapQuery : IRequest<RoadMapDto>
    {
        public string AdministratorId { get; set; }
        public int RoadMapId { get; set; }
    }
}
