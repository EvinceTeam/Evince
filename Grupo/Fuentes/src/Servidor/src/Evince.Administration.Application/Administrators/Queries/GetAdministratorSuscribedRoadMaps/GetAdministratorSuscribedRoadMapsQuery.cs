﻿using MediatR;
using System.Collections.Generic;

namespace Evince.Administration.Application.Administrators.Queries.GetAdministratorSuscribedRoadMaps
{
    public class GetAdministratorSuscribedRoadMapsQuery : IRequest<IEnumerable<RoadMapDto>>
    {
        public string AdministratorId { get; set; }
    }
}
