﻿using MediatR;
using System.Collections.Generic;

namespace Evince.Administration.Application.Administrators.Queries.GetRoadMapsByNodes
{
    public class GetRoadMapsByNodesQuery : IRequest<IEnumerable<RoadMapDto>>
    {
        public string AdministratorId { get; set; }
        public int? StartNode { get; set; }
        public int? EndNode { get; set; }
    }
}
