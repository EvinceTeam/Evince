﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance;
using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.Common.Events;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Administration.Application.Administrators.Notifications
{
    public class UserWithRoleAdminCreatedNotificationHandler : INotificationHandler<UserWithRoleAdminCreatedNotification>
    {
        private readonly IAdministratorRepository _administratorRepository;

        public UserWithRoleAdminCreatedNotificationHandler(IAdministratorRepository administratorRepository)
        {
            _administratorRepository = administratorRepository;
        }

        public async Task Handle(UserWithRoleAdminCreatedNotification notification, CancellationToken cancellationToken)
        {
            var administrator = new Administrator(notification.Id, notification.Name, notification.Lastname);

            await _administratorRepository.AddAsync(administrator, cancellationToken);
        }
    }
}
