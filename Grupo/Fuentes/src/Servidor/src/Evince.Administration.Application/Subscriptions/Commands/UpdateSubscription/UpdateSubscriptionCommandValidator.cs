﻿using FluentValidation;

namespace Evince.Administration.Application.Subscriptions.Commands.UpdateSubscription
{
    public class UpdateSubscriptionCommandValidator : AbstractValidator<UpdateSubscriptionCommand>
    {
        public UpdateSubscriptionCommandValidator()
        {
            RuleFor(x => x.AdministratorId)
                .NotNull().WithMessage("El identificador del administrador no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador del administrador no puede estar vacío");
            RuleFor(x => x.RoadmapId)
                .NotNull().WithMessage("El identificador del recorrido no puede tener un valor nulo");
        }
    }
}
