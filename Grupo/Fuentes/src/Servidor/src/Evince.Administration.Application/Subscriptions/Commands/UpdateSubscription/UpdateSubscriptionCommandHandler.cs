﻿using Evince.Administration.Domain.Entities;
using Evince.Application.Interfaces.Persistance.EntitiesRepositories;
using Evince.Application.Specifications;
using Evince.PassengersTransport.Domain.Entities;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Evince.Administration.Application.Subscriptions.Commands.UpdateSubscription
{
    public class UpdateSubscriptionCommandHandler : IRequestHandler<UpdateSubscriptionCommand, Unit>
    {
        private readonly ISubscriptionRepository _subscriptionRepository;
        private readonly IAdministratorRepository _administratorRepository;
        private readonly IRoadMapRepository _roadMapRepository;

        public UpdateSubscriptionCommandHandler(
            ISubscriptionRepository subscriptionRepository,
            IAdministratorRepository administratorRepository,
            IRoadMapRepository roadMapRepository
        )
        {
            _subscriptionRepository = subscriptionRepository;
            _administratorRepository = administratorRepository;
            _roadMapRepository = roadMapRepository;
        }

        public async Task<Unit> Handle(UpdateSubscriptionCommand request, CancellationToken cancellationToken)
        {
            var administrator = await GetAdministratorAsync(request.AdministratorId, cancellationToken);

            if (administrator == null)
            {
                throw new InvalidOperationException("No ha sido posible encontrar al administrador seleccionado");
            }

            var roadmap = await GetRoadMapAsync(request.RoadmapId, cancellationToken);

            if (roadmap == null)
            {
                throw new InvalidOperationException("No ha sido posible encontrar el recorrido seleccionado");
            }

            var subscription = administrator.Subscriptions.FirstOrDefault(s => s.RoadMap.Id == roadmap.Id);

            if (subscription == null)
            {
                var newSubscription = new Subscription(administrator.Id, roadmap);

                administrator.AddSubscription(newSubscription);

                await _administratorRepository.SaveChangesAsync(cancellationToken);
            }
            else
            {
                administrator.RemoveSubscription(subscription);

                await _subscriptionRepository.DeleteAsync(subscription, cancellationToken);
            }

            return Unit.Value;
        }

        private Task<Administrator> GetAdministratorAsync(string administratorId, CancellationToken cancellationToken)
        {
            return _administratorRepository.GetSingleBySpecAsync(new AdministratorsWithSubscriptionsSpecification(administratorId), cancellationToken);
        }

        private Task<RoadMap> GetRoadMapAsync(int roadmapId, CancellationToken cancellationToken)
        {
            return _roadMapRepository.GetByIdAsync(roadmapId, cancellationToken);
        }
    }
}
