﻿using MediatR;

namespace Evince.Administration.Application.Subscriptions.Commands.UpdateSubscription
{
    public class UpdateSubscriptionCommand : IRequest
    {
        public string AdministratorId { get; set; }
        public int RoadmapId { get; set; }
    }
}
