﻿namespace Evince.Common.Entities
{
    public abstract class BaseEntity<TId>
    {
        protected BaseEntity()
        {
        }

        public TId Id { get; set; }
    }
}
