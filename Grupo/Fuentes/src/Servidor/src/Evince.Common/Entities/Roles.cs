﻿namespace Evince.Common.Entities
{
    public enum Roles
    {
        Admin = 1,
        Driver = 2,
        Consultant = 3
    }
}
