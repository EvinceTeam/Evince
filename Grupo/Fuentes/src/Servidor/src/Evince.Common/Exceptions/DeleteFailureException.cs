﻿using System;

namespace Evince.Common.Exceptions
{
    public class DeleteFailureException : Exception
    {
        public DeleteFailureException(string name, object key, string message)
            : base($"Eliminación de la entidad \"{name}\" ({key}) fallida. {message}")
        {
        }
    }
}
