﻿using MediatR;

namespace Evince.Common.Events
{
    public class UserWithRoleDriverCreatedNotification : INotification
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
    }
}
