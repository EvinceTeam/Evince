﻿using FluentValidation;

namespace Evince.Common.Events
{
    public class UserWithRoleAdminCreatedNotificationValidator : AbstractValidator<UserWithRoleAdminCreatedNotification>
    {
        public UserWithRoleAdminCreatedNotificationValidator()
        {
            RuleFor(x => x.Id)
                .NotNull().WithMessage("El identificador no puede tener un valor nulo")
                .NotEmpty().WithMessage("El identificador no puede estar vacío");
            RuleFor(x => x.Lastname)
                .NotNull().WithMessage("El apellido no puede tener un valor nulo")
                .NotEmpty().WithMessage("El apellido no puede estar vacío");
            RuleFor(x => x.Name)
                .NotNull().WithMessage("El nombre no puede tener un valor nulo")
                .NotEmpty().WithMessage("El nombre no puede estar vacío");
        }
    }
}
