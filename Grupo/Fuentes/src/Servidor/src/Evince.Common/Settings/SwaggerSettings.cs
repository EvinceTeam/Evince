﻿namespace Evince.Common.Settings
{
    public class SwaggerSettings
    {
        public string JsonRoute { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string RoutePrefix { get; set; }
        public string UIEndpoint { get; set; }
        public string Version { get; set; }
    }
}
