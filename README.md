# Evince :bar_chart:

[![pipeline status](https://gitlab.com/EvinceTeam/Evince/badges/master/pipeline.svg)](https://gitlab.com/EvinceTeam/Evince/-/commits/master)

Evince es un proyecto destinado para realizar reportes :chart_with_upwards_trend: sobre los viajes de colectivos de la empresa F.B :oncoming_bus:

## Introducción :rocket:

Las siguientes instrucciones lo ayudarán a obtener una copia funcional del proyecto en su máquina local.

### Requisitos previos :grey_exclamation:

#### Servidor

Cabe destacar que la aplicación pretende, por el momento :sweat_smile:, funcionar sobre sistemas operativos Windows. Esto puede ser una limitación en algunos casos pero creemos en Evince Team que actualmente no es necesario compatibilizar con otros sistemas opertivos.

Además, es condición necesaria que quienes pretendan utilizar la aplicación sean administradores de sus equipos ya que se realizarán operaciones durante la instalación que necesitan privilegios de administrador.

### Instalación :wrench:

#### Servidor

![NetCore](https://doggy8088.gallerycdn.vsassets.io/extensions/doggy8088/netcore-snippets/0.2.2/1534177640486/Microsoft.VisualStudio.Services.Icons.Default)

En primer lugar es necesario descargar el [.NET Core 2.2 SDK](https://dotnet.microsoft.com/download)

**Instrucciones**
1.  Abrir la consola en modo administrador y ubicarse en el folder ..\Evince\Grupo\Fuentes\src\Servidor\Servidor.CoreFramework
2.  Ejecutar los comandos
    
    `dotnet restore`

    `dotnet build`
3.  Correr aplicación con el comando
    
    `dotnet run`

## Despliegue

Add additional notes about how to deploy this on a live system

## Tecnologías utilizadas

### Servidor
* [Visual Studio](https://www.visualstudio.com/) - IDE de desarrollo.
* [Microsoft .NET Framework 4.5.2](https://www.microsoft.com/en-us/download/details.aspx?id=42643&desc=dotnet452) - Framework .NET utilizado.
* [ASP .NET Web API](https://www.asp.net/web-api) - Utilizado para comunicar los proyectos cliente y servidor.
* [Microsoft SQL Server 2014 Express](https://www.microsoft.com/es-ar/download/details.aspx?id=42299) - Utilizado como motor de base de datos
* [NHibernate](http://nhibernate.info/) - Utilizado para mapear los objetos con la base de datos.
* [Ninject](http://www.ninject.org/) - Utilizado para DI y IoC
* [log4net](http://logging.apache.org/log4net/) - Utilizado para el logging

## Autores

* **Germán Asmus** - *Cliente* - [Germán](https://gitlab.com/AsmusGerman)
* **Silvana García** - *Servidor* - [Silvana](https://gitlab.com/silvanaBGN)
* **Facundo Rodríguez** - *Servidor* - [Facundo](https://gitlab.com/facurodriguez)
* **Alejandro Segovia** - *Cliente* - [Alejandro](https://gitlab.com/alesegovia07)

Ve también quienes han [colaborado](https://gitlab.com/EvinceTeam/Evince/graphs/master) para hacer este proyecto posible.

## Agradecimientos

* ?

